
<%@taglib uri="http://www.springframework.org/tags" prefix="local"%>


<h1 align="left">
					<local:message code="label.NewsPortal" />
				</h1>
				

				<div align="right">
					 <a href="?lang=en&<%=request.getQueryString() %>"><local:message code="label.en" /></a> 
					 <a href="?lang=ru&<%=request.getQueryString() %>"><local:message code="label.ru" /></a>
				</div>		
