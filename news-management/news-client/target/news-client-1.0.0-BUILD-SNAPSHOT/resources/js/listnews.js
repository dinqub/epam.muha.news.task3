function builderTags() {
	var list = document.getElementsByName("tagIdList");
	var i = 0;
	var c = "";
	var count = 0;
	for (i = 0; i < list.length; ++i) {
		if (list[i].checked) {
			c += list[i].id + " ";
			count++;
		}
	}
	if (count != 0) {
		$$('tag', c);
	} else {
		var tag = document.getElementsByName("tagloc");
		$$('tag', tag[0].value);
	}
}

$$r(function() {
	
	builderTags();
	
	var para = document.getElementById("bodyId");
	var button = document.getElementById("selectBox");
	var check = document.getElementById("checkboxes");

	para.addEventListener("mousedown", function(event){
		 disapearCheckboxes();
	});
	button.addEventListener("mousedown", function(event) {
		showCheckboxes();
	  	event.stopPropagation();
	});
		check.addEventListener("mousedown", function(event) {
	  	event.stopPropagation();
	}); 
	
});