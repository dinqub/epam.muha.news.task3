package com.epam.newsmanagement;

import java.util.ResourceBundle;

public class ConfigurationManager {

	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("prop");

	public ConfigurationManager() {	}


	public static String getProperty(String key) {
		
		return resourceBundle.getString(key);
	
	}
}