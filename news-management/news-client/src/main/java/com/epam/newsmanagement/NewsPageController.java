package com.epam.newsmanagement;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Filter;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Page;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.INewsManagerService;

@Controller
public class NewsPageController extends ErrorController{
	

	@Autowired
	public  INewsManagerService newsManagerService;

	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String first(Locale locale, Model model, HttpServletRequest request) {

		Locale.setDefault(Locale.ENGLISH);

		return "redirect:/listnews";
	}
	
	@RequestMapping(value = "/listnews", method = RequestMethod.GET)
	public String listNews(Model model,
			@RequestParam(value = "pageId", required = false) Long pageId,
			HttpServletRequest request) throws ServiceException {

		Filter filter = (Filter) request.getSession().getAttribute("filter");
		
		if (filter == null ) {
			filter=new Filter();
			request.getSession().setAttribute("filter",filter);
		}
		if(pageId==null){
			filter=new Filter();
			request.getSession().setAttribute("filter",filter);
			pageId=1L;
		}
		
		Page page=new Page();
		page.setSize(Long.parseLong(ConfigurationManager.getProperty("pageCount")));
		
	
		List<News> news = newsManagerService.getFilterNewsList(filter, pageId,page.getSize());
		System.out.println(news);
		page.setNews(news);
		page.setPage(pageId);
		page.setTotalSize(newsManagerService.getCountOfFilterPage(filter));

		model.addAttribute("page", page);
		List<Author> authors=newsManagerService.getAllAuthors();
		List<Tag> tags= newsManagerService.getAllTags();
		model.addAttribute("authors", authors);
		model.addAttribute("tags",tags);

		model.addAttribute("filter", filter);

		return "listnews";
	}

	@RequestMapping(value = "/reset", method = RequestMethod.GET)
	public String reset(Model model,RedirectAttributes redirectAttributes,
			@RequestParam(value = "pageId", required = false) long pageId,
			HttpServletRequest request) throws ServiceException {
	
		Filter filter = new Filter();

		request.getSession().setAttribute("filter", filter);
		redirectAttributes.addAttribute("pageId", 1L);
		
		return "redirect:/listnews";
	}

	@RequestMapping(value = "/filter", method = RequestMethod.POST)
	public String filter(
			Model model,
			RedirectAttributes redirectAttributes,
			@RequestParam(value = "authorId", required = false) long authorId,
			@RequestParam(value = "tagIdList", required = false) List<Long> tagIdList,
			HttpServletRequest request) throws ServiceException {

		redirectAttributes.addAttribute("pageId", 1L);

		Filter filter = new Filter(authorId, tagIdList);

		request.getSession().setAttribute("filter", filter);

		return "redirect:/listnews";
	}

}
