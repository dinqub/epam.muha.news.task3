package com.epam.newsmanagement;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.entity.Filter;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.INewsManagerService;

@Controller
public class NewsController extends ErrorController{


	@Autowired
	public  INewsManagerService newsManagerService;
	
	final String NEXT="next";
	final String PREV="prev";

	@RequestMapping(value = "/changenews", method = RequestMethod.GET)
	public String changeNews(
			RedirectAttributes redirectAttributes,
			@RequestParam(value = "typeOfChange", required = false) String typeOfChange,
			@RequestParam(value = "newsId", required = false) long newsId,
			@RequestParam(value = "filterId", required = false) long filterId,
			Model model, HttpServletRequest request) throws ServiceException {

		Filter filter = (Filter) request.getSession().getAttribute("filter");

		if (typeOfChange.equals(NEXT)) {

			News news = newsManagerService.getFilterNews(filter, filterId + 1);
			redirectAttributes.addAttribute("filterId", filterId + 1);
			redirectAttributes.addAttribute("newsId", news.getId());

		}
		if (typeOfChange.equals(PREV)) {

			News news = newsManagerService.getFilterNews(filter, filterId - 1);
			redirectAttributes.addAttribute("filterId", filterId - 1);
			redirectAttributes.addAttribute("newsId", news.getId());

		}

		return "redirect:/newsview";
	}

	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public String news(
			RedirectAttributes redirectAttributes,
			@RequestParam(value = "newsId", required = false) Long newsId,
			@RequestParam(value = "pageId", required = false) Long pageId,
			@RequestParam(value = "newsOnPage", required = false) Long newsOnPage,
			@RequestParam(value = "rowOnPageId", required = false) int rowOnPageId,
			Model model) throws ServiceException {

		long filterId = (pageId - 1) * newsOnPage + rowOnPageId;

		redirectAttributes.addAttribute("filterId", filterId);
		redirectAttributes.addAttribute("newsId", newsId);

		return "redirect:/newsview";
	}

	@RequestMapping(value = "/newsview", method = RequestMethod.GET)
	public String paintNews(Model model, HttpServletRequest request,
			@RequestParam(value = "newsId", required = false) long newsId,
			@RequestParam(value = "filterId", required = false) long filterId)
			throws ServiceException {

		Filter filter = (Filter) request.getSession().getAttribute("filter");
		model.addAttribute("total_news",
				newsManagerService.getCountOfFilterPage(filter));
		model.addAttribute("newId", newsId);
		model.addAttribute("filterId", filterId);
		model.addAttribute("newsVO", newsManagerService.getNews(newsId));

		return "newsview";
	}

	



}
