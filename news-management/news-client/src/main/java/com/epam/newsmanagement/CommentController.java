package com.epam.newsmanagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ICommentService;

@Controller
public class CommentController extends ErrorController {
	
	@Autowired
	public ICommentService commentService;
	
	@RequestMapping(value = "/commenting", method = RequestMethod.POST)
	public String commenting(
			RedirectAttributes redirectAttributes,
			@RequestParam(value = "newsId", required = false) Long newsId,
			@RequestParam(value = "filterId", required = false) long filterId,
			@RequestParam(value = "commentText", required = false) String commentText,
			Model model) throws ServiceException {

		if (!commentText.equals("")) {
			Comment comment = new Comment(commentText, newsId);
			commentService.add(comment);
		}
		redirectAttributes.addAttribute("filterId", filterId);
		redirectAttributes.addAttribute("newsId", newsId);

		return "redirect:/newsview";
	}

}
