<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<fmt:requestEncoding value="utf-8" />
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="resource.pagecontent" />
<%@taglib uri="http://www.springframework.org/tags" prefix="local"%>
<html>
<head>
<title>News portal</title>
<link href="<s:url value="/resources/css/login.css"/>" rel="stylesheet" />
</head>
<body>
	<table border="2" bordercolor="black" rules="rows" width="100%"
		height="100%" cellpadding="4">
		<tr height="5%">
			<td>
				<h1 align="left" style="color: blue; margin-left: 21px;">
			<local:message code="label.NewsPortal" />
		</h1>
			
			</td>
		</tr>
		<tr>
			<td>
				<table border="3" bordercolor="grey" rules="cols" width="100%"
					height="100%">
					<tr height="29%">
						<td></td>
					</tr>
					<tr>
						<td><div align="center" >
							<h1><local:message code="label.PageNotFound" /></h1>
							</div>
						</td>
					</tr>
					<tr align="center">
						<td></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr height="1%">
			<td>
				<div align="center">
					<a>Copyright @ Epam 2015. All rights reserved.</a>
				</div>
			</td>
		</tr>

	</table>
</body>
</html>


