package com.epam.muha.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.service.impl.CommentService;


@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {
	
	@Mock
	private ICommentDAO mockedCommentDAO;	
	private static Comment comment;
	@InjectMocks
	private ICommentService commentService=new CommentService();
	
	@BeforeClass
	public static void setUp(){
		comment = new Comment();		
	}
	

	@Test
	public void testAdd() throws ServiceException, DAOException {
		commentService.add(comment);
		verify(mockedCommentDAO).add(comment);
	}

	@Test
	public void testRemove() throws ServiceException, DAOException {
		long expected=1L;
		commentService.remove(expected);
		verify(mockedCommentDAO).remove(expected);
	}	

	@Test
	public void testGet() throws ServiceException, DAOException{
		long expected=2L;
		when(mockedCommentDAO.get(expected)).thenReturn(comment);
		commentService.get(expected);
		verify(mockedCommentDAO).get(expected);
		assertEquals(comment, commentService.get(expected));

	}

	@Test
	public void testUpdate() throws ServiceException, DAOException{
		commentService.update(comment);
		verify(mockedCommentDAO).update( comment);

	}

	@Test(expected=DAOException.class)
	 public void testAddException() throws DAOException{
	 doThrow(new DAOException("Can't create Comment")).when(mockedCommentDAO).add(null);
	 mockedCommentDAO.add(null);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testAddLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't create Comment")).when(mockedCommentDAO).add(null);
	 commentService.add(null);
	 }
	 
	 @Test(expected=DAOException.class)
	 public void testRemoveDAOException() throws DAOException{
	 doThrow(new DAOException("Can't delete News")).when(mockedCommentDAO).remove(0L);
	 mockedCommentDAO.remove(0L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testRemoveLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't delete Comment")).when(mockedCommentDAO).remove(0L);
	 commentService.remove(0L);
	 }
	 
	 @Test(expected=DAOException.class)
	 public void testGetDAOException() throws DAOException{
	 doThrow(new DAOException("Can't read Comment")).when(mockedCommentDAO).get(1L);
	 mockedCommentDAO.get(1L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testGetLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't read Comment")).when(mockedCommentDAO).get(1L);
	 commentService.get(1L);
	 }
	
	 @Test(expected=DAOException.class)
	 public void testUpdateDAOException() throws DAOException{
	 doThrow(new DAOException("Can't update Comment")).when(mockedCommentDAO).update(comment);
	 mockedCommentDAO.update(comment);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testUpdateLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't update Comment")).when(mockedCommentDAO).update(comment);
	 commentService.update(comment);
	 }

}
