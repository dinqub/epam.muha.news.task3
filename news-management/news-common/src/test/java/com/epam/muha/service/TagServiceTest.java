package com.epam.muha.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.impl.TagService;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

	@Mock
	private ITagDAO mockedTagDAO;
	private static Tag tag;
	@InjectMocks
	private ITagService tagService= new TagService();

	@BeforeClass
	public static void setUp() {
		tag = new Tag();
	}

	@Test
	public void testAdd() throws DAOException, ServiceException {
		tagService.add(tag);
		verify(mockedTagDAO).add(tag);
}

	@Test
	public void testRemove() throws ServiceException, DAOException{
		long expected = 1L;
		tagService.remove(expected);
		verify(mockedTagDAO).remove(expected);
	}

	@Test
	public void testUpdate() throws DAOException, ServiceException {
		tagService.update(tag);
		verify(mockedTagDAO).update(tag);

	}

	@Test
	public void testGet() throws DAOException, ServiceException {
		long expected =0L;
		when(mockedTagDAO.get(expected)).thenReturn(tag);
		tagService.get(expected);
	verify(mockedTagDAO).get(expected);
		assertEquals(tag, mockedTagDAO.get(expected));
		assertEquals(tag, tagService.get(expected));

	}
	


	 @Test(expected=DAOException.class)
	 public void testAddException() throws DAOException{
	 doThrow(new DAOException("Can't create tag")).when(mockedTagDAO).add(null);
     mockedTagDAO.add(null);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testAddLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't create tag")).when(mockedTagDAO).add(null);
	 tagService.add(null);
	 }
	 	 
	 @Test(expected=DAOException.class)
	 public void testRemoveDAOException() throws DAOException{
	 doThrow(new DAOException("Can't delete News")).when(mockedTagDAO).remove(0L);
	 mockedTagDAO.remove(0L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testRemoveLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't delete Tag")).when(mockedTagDAO).remove(0L);
	 tagService.remove(0L);
	 }
	 		
	 @Test(expected=DAOException.class)
	 public void testGetDAOException() throws DAOException{
	 doThrow(new DAOException("Can't read Tag")).when(mockedTagDAO).get(1L);
	 mockedTagDAO.get(1L);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testGetLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't read Tag")).when(mockedTagDAO).get(1L);
	 tagService.get(1L);
	 }
	
		
	 
	 @Test(expected=DAOException.class)
	 public void testUpdateDAOException() throws DAOException{
	 doThrow(new DAOException("Can't update Tag")).when(mockedTagDAO).update(tag);
	 mockedTagDAO.update(tag);
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testUpdateLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't update Tag")).when(mockedTagDAO).update(tag);
	 tagService.update(tag);
	 }
	
	 @Test(expected=DAOException.class)
	 public void testGetAllDAOException() throws DAOException{
	 doThrow(new DAOException("Can't get all Tag")).when(mockedTagDAO).getAll();
	 mockedTagDAO.getAll();
	 }
	 
	 @Test(expected=ServiceException.class)
	 public void testGetAllLogicException() throws  ServiceException, DAOException{
	 doThrow(new DAOException("Can't get all Tag")).when(mockedTagDAO).getAll();
	 tagService.getAll();
	 }

}
