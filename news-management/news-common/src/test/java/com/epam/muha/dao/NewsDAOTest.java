package com.epam.muha.dao;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.dao.impl.AuthorDAO;
import com.epam.newsmanagement.dao.impl.CommentDAO;
import com.epam.newsmanagement.dao.impl.NewsDAO;
import com.epam.newsmanagement.dao.impl.TagDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:springpropertiestest.xml" })
public class NewsDAOTest {

	@Autowired
	@Qualifier("newsDAO")
	private static INewsDAO newsDAO;
	@Autowired
	@Qualifier("tagDAO")
	private static ITagDAO tagDAO ;
	@Autowired
	@Qualifier("commentDAO")
	private static ICommentDAO commentDAO;
	@Autowired
	@Qualifier("authorDAO")
	private static IAuthorDAO authorDAO;
	
	
	public static java.util.Date today = new java.util.Date();
	public static Timestamp date=new Timestamp(today.getTime());
	
	private static News news1;
	private static News news2;
	private static News news3;
	private static News news4;
		
	private static Tag tag1;
	private static Tag tag2;
	private static Tag tag3;
	
	private static Comment comment1;
	private static Comment comment2;
	private static Comment comment3;
	private static Comment comment4;
	
	private static Author author1;
	private static Author author2;

	@BeforeClass
	public static void setUpDao() throws DAOException {
		@SuppressWarnings("resource")
		ClassPathXmlApplicationContext cx = new ClassPathXmlApplicationContext("springpropertiestest.xml");
		Locale.setDefault(Locale.ENGLISH);
		commentDAO=(CommentDAO) cx.getBean("commentDAO");
		newsDAO=(NewsDAO) cx.getBean("newsDAO");
		authorDAO=(AuthorDAO) cx.getBean("authorDAO");
		tagDAO=(TagDAO) cx.getBean("tagDAO");
		tag1 = new Tag(1L,"Test1");
		tag2 = new Tag(2L,"Test2");
		tag3 = new Tag(3L,"Test3");
		
		comment1=new Comment(1L,"Comment1",date,1);
		comment2=new Comment(2L,"Comment2",date,1);
		comment3=new Comment(3L,"Comment3",date,2);
		comment4=new Comment(4L,"Comment4",date,2);
		
		author1 = new Author(1L,"Test1");
		author2 = new Author(2L,"Test2");
		
		Date dates=new Date(today.getTime());
		news1=new News(1L,"Test1","Test1","Test1",date,dates);
		news2=new News(2L,"Test2","Test2","Test2",date,dates);
		news3=new News(3L,"Test3","Test3","Test3",date,dates);
		news4=new News(4L,"Test4","Test4","Test4",date,dates);
		
		UtilitTest.createSequence(newsDAO);
		newsDAO.add(news1);
		newsDAO.add(news2);
		newsDAO.add(news3);
		
		UtilitTest.createSequence(tagDAO);
		tagDAO.add(tag1);
		tagDAO.add(tag2);
		tagDAO.add(tag3);
		
    	tagDAO.add(tag1.getId(),1L);
		tagDAO.add(tag2.getId(),1L);
		tagDAO.add(tag2.getId(),2L);
		
		UtilitTest.createSequence(commentDAO);
		commentDAO.add(comment1);
		commentDAO.add(comment2);
		commentDAO.add(comment3);
		commentDAO.add(comment4);
		
		UtilitTest.createSequence(authorDAO);
		authorDAO.add(author1);
		authorDAO.add(author2);
		
		authorDAO.add(author1.getId(),1L);
		authorDAO.add(author2.getId(),2L);
	}

	@AfterClass
	public static void cleanDAO() throws DAOException {
		UtilitTest.deleteSequence(commentDAO);
		List<Comment> comments = commentDAO.getAll(1L);
		for (Comment comment : comments) {
			commentDAO.remove(comment.getId());
		}
	    comments = commentDAO.getAll(2L);
		for (Comment comment : comments) {
			commentDAO.remove(comment.getId());
		}
		UtilitTest.deleteSequence(tagDAO);
		tagDAO.remove(1L);
		tagDAO.remove(2L);
		tagDAO.remove(3L);
		
		UtilitTest.deleteSequence(authorDAO);
		authorDAO.delete(1L);
		authorDAO.delete(2L);
		
		UtilitTest.deleteSequence(newsDAO);
		newsDAO.remove(1L);
		newsDAO.remove(2L);
		newsDAO.remove(3L);
		newsDAO.remove(4L);
	}
	

	@Test
	public void testAdd() throws DAOException {
		long id = newsDAO.add(news4);
		News result = newsDAO.get(id);
		assertEquals(news4,result);
	}

	@Test
	public void testRemove() throws DAOException {
		newsDAO.remove(3L);
		List<News> temp = newsDAO.getAllNews();
		assertEquals(false, temp.contains(news3));
	}

	@Test
	public void testGet() throws DAOException {
		News result = newsDAO.get(1L);
		assertEquals(news1, result);
	}
	
	@Test
	public void testUpdate() throws DAOException {
		news1.setTitle("Test11");
		newsDAO.update(news1);
		News result=newsDAO.get(1L);
		assertEquals(news1,result);
	}
	
	@Test
	public void testGetAll() throws DAOException {
		List<News> temp = newsDAO.getAllNews();
		assertEquals(true, temp.contains(news1));
		assertEquals(true, temp.contains(news2));
	}
	
	@Test
	public void testSearchByAuthor() throws DAOException {
		List<News> temp = newsDAO.searchByAuthor(1L);
		assertEquals(true, temp.contains(news1));
	}

	@Test
	public void testSearchByTags() throws DAOException {
		List<Tag> tags=Arrays.asList(tag1,tag2);
		List<News> temp = newsDAO.searchByTags(tags);
		assertEquals(true, temp.contains(news1));
		assertEquals(true, temp.contains(news2));
		assertEquals(2L,temp.size());
	}
	
}
