package com.epam.muha.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.service.INewsManagerService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.service.impl.NewsManagerService;

@RunWith(MockitoJUnitRunner.class)
public class NewsManagerServiceTest {

	
	@Mock
	private ITagService mokedTagService;
	@Mock
	private IAuthorService mokedAuthorService;
	@Mock
	private INewsService mokedNewsService;
	@Mock
	private ICommentService mokedCommentService;
	
	private static Set<Author> author;	
	private static News news;	
	private static Tag tag;	
	private static Author autho;	
	private static Set<Tag> tags;
	private static List<Long> tagIdList;
	private static Long authorId;
	
	@InjectMocks
	private INewsManagerService newsManagerService= new NewsManagerService();

	@BeforeClass
	public static void setUp() {
		tag=new Tag();
		autho=new Author();
		author=new HashSet<Author>();
		tags=new HashSet<Tag>();
		news=new News();
		tagIdList=new ArrayList<Long>();
		authorId=new Long(0);
		news.setAuthor(author);
		news.setTags(tags);
	}
				
	@Test
	public void testAddNews() throws ServiceException, ServiceException {
		when(mokedTagService.get(0L)).thenReturn(tag);
		authorId =0L;
		when(mokedAuthorService.get(authorId)).thenReturn(autho);
		newsManagerService.addNews(news,Arrays.asList(0L),authorId);
		verify(mokedNewsService).add(news);
		verify(mokedAuthorService).get(authorId);
	}
	@Test
	public void testRemoveNewsByListOfId() throws ServiceException, ServiceException {		
		newsManagerService.removeNews(tagIdList);
		verify(mokedNewsService).remove(tagIdList);
	}

	@Test
	public void testGetNews() throws ServiceException , ServiceException{
		long expected = news.getId();
		when(mokedNewsService.get(expected)).thenReturn(news);
		newsManagerService.getNews(expected);
	    verify(mokedNewsService).get(expected);

		assertEquals(news, newsManagerService.getNews(expected));
		assertEquals(news.getAuthor(), newsManagerService.getNews(expected).getAuthor());
		assertEquals(news.getComments(), newsManagerService.getNews(expected).getComments());
		assertEquals(news.getTags(), newsManagerService.getNews(expected).getTags());

	}

	@Test
	public void testUpdateNews() throws ServiceException, ServiceException {
		authorId = 0L;
		when(mokedNewsService.get(news.getId())).thenReturn(news);
		newsManagerService.updateNews(news,Arrays.asList(0L),0L);
		verify(mokedTagService).getTagsById(Arrays.asList(0L));
		verify(mokedAuthorService).get(authorId);
		verify(mokedNewsService).update(news);
	}

    @Test(expected=ServiceException.class)
    public void testAddNewsServiceException() throws ServiceException{
        doThrow(new ServiceException("Can't create News")).when(mokedNewsService).add(null);
		mokedNewsService.add(null);
	}
				 
     	 
	 @Test(expected=ServiceException.class)
	 public void testRemoveNewsServiceException() throws ServiceException{
		 doThrow(new ServiceException("Can't delete News")).when(mokedNewsService).remove(0L);
		 mokedNewsService.remove(0L);
	 }
				 
	 @Test(expected=ServiceException.class)
	 public void testGetNewsServiceException() throws ServiceException{
		 doThrow(new ServiceException("Can't read News")).when(mokedNewsService).get(-1L);
		 mokedNewsService.get(-1L);
	 }
				 
	 @Test(expected=ServiceException.class)
		 public void testGetNewsLogicException() throws  ServiceException, ServiceException{
		 doThrow(new ServiceException("Can't read News")).when(mokedNewsService).get(-1L);
		 newsManagerService.getNews(-1L);
	 }
				
	 @Test(expected=ServiceException.class)
	 public void testUpdateNewsServiceException() throws ServiceException{
		 doThrow(new ServiceException("Can't update News")).when(mokedNewsService).update(null);
		 mokedNewsService.update(null);
	 }
				 
	 @Test(expected=ServiceException.class)
		 public void testUpdateNewsLogicException() throws  ServiceException, ServiceException{
		 when(mokedNewsService.get(news.getId())).thenReturn(news);
		 when(mokedTagService.get(3L)).thenReturn(tag);
		 when(mokedAuthorService.get(0L)).thenReturn(autho);	
		 doThrow(new ServiceException("Can't update News")).when(mokedNewsService).update(news);
		 newsManagerService.updateNews(news,Arrays.asList(3L),0L);
	 }
				
	 @Test(expected=ServiceException.class)
	 public void testGetAllNewsServiceException() throws ServiceException{
		 doThrow(new ServiceException("Can't get all News")).when(mokedNewsService).getAllNews();
		 mokedNewsService.getAllNews();
	 }
				 
	 @Test(expected=ServiceException.class)
	 public void testGetAllNewsLogicException() throws  ServiceException, ServiceException{
		 doThrow(new ServiceException("Can't get all News")).when(mokedNewsService).getAllNews();
		 newsManagerService.getSortedNews();
	 }
				 
}
