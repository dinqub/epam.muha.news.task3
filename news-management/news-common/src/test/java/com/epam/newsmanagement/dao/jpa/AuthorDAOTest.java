package com.epam.newsmanagement.dao.jpa;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.unitils.UnitilsJUnit4;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:springTestJpa.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
						  DbUnitTestExecutionListener.class })
public class AuthorDAOTest extends UnitilsJUnit4 {

	@Autowired
	@Qualifier("authorDAOJpa")
	private IAuthorDAO authorDAO;
	
	@BeforeClass
	public static void setUpDao() throws DAOException {
		Locale.setDefault(Locale.ENGLISH);
	}
	
	@Test
	@DatabaseSetup(value="classpath:AuthorDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:AuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testAddAuthor() throws Exception {		
		Author c = new Author("author4");	
		authorDAO.add(c);
		List<Author> list=authorDAO.getAll();
		assertEquals(true, list.contains(c));
	}
	
	@Test
	@DatabaseSetup(value="classpath:AuthorDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:AuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGet() throws Exception {			
		Author author=authorDAO.get(2L);		
		assertEquals(author.getId(), 2L);
		assertEquals(author.getName(), "Author2");		
	}
	
	
	@Test
	@DatabaseSetup(value="classpath:AuthorDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:AuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetAll() throws Exception {		
		
		List<Author> list=authorDAO.getAll();
		assertEquals(3, list.size());
		
	}
	
	@Test
	@DatabaseSetup(value="classpath:AuthorDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:AuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testUpdate() throws Exception {

		Author author = authorDAO.get(3L);
		author.setName("new author");
		authorDAO.update(author);
		
		Author newauthor = authorDAO.get(3L);
		
		assertEquals(author.getId(), newauthor.getId());
		assertEquals(author.getName(), newauthor.getName());
	}

	@Test
	@DatabaseSetup(value="classpath:AuthorDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:AuthorDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testExpire() throws Exception {

		authorDAO.expired(1L);
		Author author=authorDAO.get(1L);
		System.out.println(author.getExpired());
		
		assertNotNull(author.getExpired());
	}


}