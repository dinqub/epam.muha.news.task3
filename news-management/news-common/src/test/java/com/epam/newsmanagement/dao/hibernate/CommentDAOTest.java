package com.epam.newsmanagement.dao.hibernate;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import org.unitils.UnitilsJUnit4;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:springTestHibernate.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
						  DbUnitTestExecutionListener.class })

public class CommentDAOTest extends UnitilsJUnit4 {

	@Autowired
	@Qualifier("commentDAOHibernate")
	private ICommentDAO commentDAO;
	
	@BeforeClass
	public static void setUpDao() throws DAOException {
		Locale.setDefault(Locale.ENGLISH);
	}	
	
	@Test
	@DatabaseSetup(value="classpath:CommentDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value ="classpath:CommentDS.xml", type = DatabaseOperation.DELETE_ALL)
	@Transactional("transactionManager")
	public void testCreate() throws Exception {		
		Comment c = new Comment("comment",3L);
		c.setCreationDate(new Date());		
		commentDAO.add(c);
		List<Comment> list=commentDAO.getAllByNewsId(3L);
		assertEquals(true, list.contains(c));
	}
	
	@Test
	@DatabaseSetup(value="classpath:CommentDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value ="classpath:CommentDS.xml", type = DatabaseOperation.DELETE_ALL)
	@Transactional("transactionManager")
	public void testFindByNewsId() throws Exception {		
		
		List<Comment> list=commentDAO.getAllByNewsId(1L);
		assertEquals(3, list.size());
		
	}
	
	@Test
	@DatabaseSetup(value="classpath:CommentDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value ="classpath:CommentDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testUpdate() throws Exception {

		Comment comment = commentDAO.get(4L);
		comment.setCommentText("New comment");
		commentDAO.update(comment);
		
		Comment newComment = commentDAO.get(4L);
		
		assertEquals(comment.getId(), newComment.getId());
		assertEquals(comment.getCommentText(), newComment.getCommentText());
		assertEquals(comment.getNewsId(), newComment.getNewsId());
	}
	
	@Test
	@DatabaseSetup(value="classpath:CommentDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value ="classpath:CommentDS.xml", type = DatabaseOperation.DELETE_ALL)
	@Transactional("transactionManager")
	public void testDelete() throws Exception {

		commentDAO.remove(2L);
		int rows = commentDAO.getAllByNewsId(1L).size();
		assertEquals(2, rows);
	}


}