package com.epam.newsmanagement.dao.hibernate;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import org.unitils.UnitilsJUnit4;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:springTestHibernate.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
						  DbUnitTestExecutionListener.class })
@Transactional("transactionManager")
public class NewsDAOTest extends UnitilsJUnit4 {

	@Autowired
	@Qualifier("newsDAOHibernate")
	private INewsDAO newsDAO;
	
	
	@Autowired
	@Qualifier("authorDAOHibernate")
	private IAuthorDAO authorDAO;
	
	@Autowired
	@Qualifier("tagDAOHibernate")
	private ITagDAO tagDAO;
	
	
	@BeforeClass
	public static void setUpDao() throws DAOException {
		Locale.setDefault(Locale.ENGLISH);
	}
	@Test
	@DatabaseSetup(value="classpath:newsDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:newsDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testAddNews() throws Exception {		
		News news = new News("news4");	
		Author author=authorDAO.get(1);
		Set<Tag> tag=tagDAO.getTagsById(Arrays.asList(1L,2L));
		HashSet<Author> authors=new HashSet<Author>();
		authors.add(author);
		news.setAuthor(authors);
		news.setTags(tag);
		newsDAO.add(news);		
		List<News> list=newsDAO.getAllNews();
		assertEquals(4, list.size());
	}
	
	@Test
	@DatabaseSetup(value="classpath:newsDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:newsDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGet() throws Exception {			
		News news=newsDAO.get(1L);		
		assertEquals(news.getId(), 1L);	
		assertEquals(0, news.getCountComment());	
		assertEquals(3, news.getTags().size());	
		for (Author author : news.getAuthor()) {
			assertEquals("Author1", author.getName());
		}		
	}
	
	
	@Test
	@DatabaseSetup(value="classpath:newsDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:newsDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetAll() throws Exception {		
		List<News> list=newsDAO.getAllNews();
		assertEquals(3, list.size());		
	}
	
	@Test
	@DatabaseSetup(value="classpath:newsDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:newsDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testUpdate() throws Exception {

		News news = newsDAO.get(3L);
		news.setTitle("new news");
		Author author=authorDAO.get(1L);
		Set<Tag> setTag=new HashSet<Tag>();
		Tag tag1=tagDAO.get(1L);
		Tag tag3=tagDAO.get(3L);
		setTag.add(tag1);
		setTag.add(tag3);
		news.addAuthor(author);
		news.addTags(setTag);
		
		newsDAO.update(news);
		
		News newnews = newsDAO.get(3L);		
		
		assertEquals(news.getId(), newnews.getId());
		for (Author authorI : newnews.getAuthor()) {
			assertEquals(1, authorI.getId());
			assertEquals("Author1", authorI.getName());
		}		
	}

	@Test
	@DatabaseSetup(value="classpath:newsDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:newsDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testRemove() throws Exception {
		/*News news = new News("news4");	
		Author author=authorDAO.get(1);
		Set<Tag> tag=tagDAO.getTagsById(Arrays.asList(1L,2L));
		HashSet<Author> authors=new HashSet<Author>();
		authors.add(author);
		news.setAuthor(authors);
		news.setTags(tag);
		newsDAO.add(news);		
		newsDAO.remove(4L);*/
		List<News> newses=newsDAO.getAllNews();		
		assertEquals(3,newses.size());
	}
	
	@Test
	@DatabaseSetup(value="classpath:newsDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:newsDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testRemoveByList() throws Exception {
		newsDAO.remove(Arrays.asList(2L));
		List<News> news=newsDAO.getAllNews();		
		assertEquals(2,news.size());
	
	}

}