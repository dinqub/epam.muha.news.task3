package com.epam.newsmanagement.dao.hibernate;
/*package com.epam.muha.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.exception.DAOException;



public class UtilitTest {
	
	public static final String SQL_CREATE_COMMENT_SEQUENCE="CREATE SEQUENCE  COMMENT_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1";
	
	public static final String SQL_DELETE_COMMENT_SEQUENCE="DROP SEQUENCE  COMMENT_SEQ";
	
	
	public static final String SQL_CREATE_AUTHOR_SEQUENCE = "CREATE SEQUENCE  AUTHOR_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 ";

	public static final String SQL_CREATE_NEWS_SEQUENCE ="CREATE SEQUENCE  NEWS_AUTHOR_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1";
	
	public static final String SQL_DELETE_AUTHOR_SEQUENCE="DROP SEQUENCE  AUTHOR_SEQ";
	
	public static final String SQL_DELETE_NEWS_SEQUENCE="DROP SEQUENCE  NEWS_AUTHOR_SEQ";
	

	public static final String SQL_CREATE_TAG_SEQUENCE = "CREATE SEQUENCE  TAG_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1";

	public static final String SQL_CREATE_NEWS_TAG_SEQUENCE = "CREATE SEQUENCE  NEWS_TAG_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1";

	public static final String SQL_DELETE_TAG_SEQUENCE = "DROP SEQUENCE TAG_SEQ";

	public static final String SQL_DELETE_TAG_NEWS_SEQUENCE = "DROP SEQUENCE  NEWS_TAG_SEQ";
	
	
	public static final String SQL_CREATE_NEWS_VO_SEQUENCE ="CREATE SEQUENCE  NEWS_SEQ  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1";
	
	public static final String SQL_DELETE_NEWS_VO_SEQUENCE ="DROP SEQUENCE  NEWS_SEQ";

	*//**
	 * Creates the COMMENT_SEQUNCE in database 
	 * @throws DAOException 
	 *//*
	public static void createSequence(ICommentDAO commentDAO) throws DAOException {
		Connection connection = null;

		(SQL_CREATE_COMMENT_SEQUENCE);
		

		
	}
	*//**
	 * Delete the COMMENT_SEQUNCE in database 
	 *
	 * @throws DAOException the technical dao exception
	 *//*
	public static void deleteSequence(ICommentDAO commentDAO) throws DAOException {
		try {
			Connection connection = commentDAO.getDataSource().getConnection();
			PreparedStatement statement = connection.prepareStatement(SQL_DELETE_COMMENT_SEQUENCE);
			statement.executeUpdate();
			DAOUtils.close(null, statement, connection);
		} catch (SQLException e) {
			throw new DAOException(e);		
			
		}		
	}
	
	*//**
	 * Creates the AUTHOR_SEQUENCE and NEWS_AUTHOR_SEQUENCE  in database
	 *
	 * @throws DAOException the technical dao exception
	 *//*
	public static void createSequence(IAuthorDAO authorDAO)throws DAOException {
		Connection connection = null;
		try {
			connection = authorDAO.getDataSource().getConnection();
			PreparedStatement statement = connection.prepareStatement(SQL_CREATE_AUTHOR_SEQUENCE);
			statement.executeUpdate();
			
			statement = connection.prepareStatement(SQL_CREATE_NEWS_SEQUENCE);
			statement.executeUpdate();
			
			DAOUtils.close(null, statement, connection);

		} catch (SQLException e) {
			throw new DAOException(e);		
			
		}
		
	}
	*//**
	 * Delete AUTHOR_SEQUENCE and NEWS_AUTHOR_SEQUENCE  in database
	 *
	 * @throws DAOException the technical dao exception
	 *//*
	public static void deleteSequence(IAuthorDAO authorDAO) throws DAOException {
		try {
			Connection connection = authorDAO.getDataSource().getConnection();
			PreparedStatement statement = connection.prepareStatement(SQL_DELETE_AUTHOR_SEQUENCE);
			statement.executeUpdate();
			
			statement = connection.prepareStatement(SQL_DELETE_NEWS_SEQUENCE);
			statement.executeUpdate();
			
			DAOUtils.close(null, statement, connection);
		} catch (SQLException e) {
			throw new DAOException(e);		
			
		}
		
	}
	*//**
	 * Creates the TAG_SEQUENCE and NEWS_TAG_SEQUNCE in database
	 *
	 * @throws DAOException
	 *             the technical dao exception
	 *//*
	public static void createSequence(ITagDAO tagDAO) throws DAOException {
		Connection connection = null;
		try {
			connection = tagDAO.getDataSource().getConnection();
			PreparedStatement statement = connection
					.prepareStatement(SQL_CREATE_TAG_SEQUENCE);
			statement.executeUpdate();

			statement = connection
					.prepareStatement(SQL_CREATE_NEWS_TAG_SEQUENCE);
			statement.executeUpdate();

			DAOUtils.close(null, statement, connection);

		} catch (SQLException e) {
			throw new DAOException(e);

		}
	}

	*//**
	 * Delete TAG_SEQUENCE and NEWS_TAG_SEQUNCE in database
	 *
	 * @throws DAOException
	 *             the technical dao exception
	 *//*
	public static void deleteSequence(ITagDAO tagDAO) throws DAOException {
		try {
			Connection connection = tagDAO.getDataSource().getConnection();
			PreparedStatement statement = connection
					.prepareStatement(SQL_DELETE_TAG_SEQUENCE);
			statement.executeUpdate();

			statement = connection
					.prepareStatement(SQL_DELETE_TAG_NEWS_SEQUENCE);
			statement.executeUpdate();

			DAOUtils.close(null, statement, connection);
		} catch (SQLException e) {
			throw new DAOException(e);

		}
	}

	*//**
	 * Creates the NEWS_SEQUNCE in database
	 * @throws DAOException 
	 *//*
	public static void createSequence(INewsDAO newsDAO) throws DAOException {
		Connection connection = null;
		try {
			connection = newsDAO.getDataSource().getConnection();
			PreparedStatement statement = connection
					.prepareStatement(SQL_CREATE_NEWS_VO_SEQUENCE);
			statement.executeUpdate();
			DAOUtils.close(null, statement, connection);

		} catch (SQLException e) {
			throw new DAOException(e);		
		}

	}

	*//**
	 * Delete NEWS_SEQUNCE in database
	 *
	 * @throws DAOException
	 *             the technical dao exception
	 *//*
	public static void deleteSequence(INewsDAO newsDAO) throws DAOException {
		try {
			Connection connection = newsDAO.getDataSource().getConnection();
			PreparedStatement statement = connection
					.prepareStatement(SQL_DELETE_NEWS_VO_SEQUENCE);
			statement.executeUpdate();
			DAOUtils.close(null, statement, connection);
		} catch (SQLException e) {
			throw new DAOException(e);		
		}
	}


}*/