package com.epam.newsmanagement.dao.hibernate;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.unitils.UnitilsJUnit4;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:springTestHibernate.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
						  DbUnitTestExecutionListener.class })
public class TagDAOTest extends UnitilsJUnit4 {

	@Autowired
	@Qualifier("tagDAOHibernate")
	private ITagDAO tagDAO;
	
	@BeforeClass
	public static void setUpDao() throws DAOException {
		Locale.setDefault(Locale.ENGLISH);
	}
	
	@Test
	@DatabaseSetup(value="classpath:tagDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:tagDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testAddTag() throws Exception {		
		Tag tag = new Tag("tag4");	
		tagDAO.add(tag);
		List<Tag> list=tagDAO.getAll();
		assertEquals(true, list.contains(tag));
	}
	
	@Test
	@DatabaseSetup(value="classpath:tagDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:tagDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGet() throws Exception {			
		Tag tag=tagDAO.get(2L);		
		assertEquals(tag.getId(), 2L);
		assertEquals(tag.getName(), "TAG2");		
	}
	
	
	@Test
	@DatabaseSetup(value="classpath:tagDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:tagDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetAll() throws Exception {		
		
		List<Tag> list=tagDAO.getAll();
		assertEquals(3, list.size());
		
	}
	
	@Test
	@DatabaseSetup(value="classpath:tagDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:tagDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testUpdate() throws Exception {

		Tag tag = tagDAO.get(3L);
		tag.setName("new tag");
		tagDAO.update(tag);
		
		Tag newtag = tagDAO.get(3L);
		
		assertEquals(tag.getId(), newtag.getId());
		assertEquals(tag.getName(), newtag.getName());
	}

	@Test
	@DatabaseSetup(value="classpath:tagDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:tagDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testExpire() throws Exception {

		tagDAO.remove(1L);
		List<Tag> tag=tagDAO.getAll();
		
		assertEquals(2,tag.size());
	}

	@Test
	@DatabaseSetup(value="classpath:tagDS.xml",type=DatabaseOperation.INSERT)
	@DatabaseTearDown(value="classpath:tagDS.xml", type = DatabaseOperation.DELETE_ALL)
	public void testGetTagsByTagsId() throws Exception {
		List<Long> tags=new ArrayList<Long>();
		tags.add(1L);
		tags.add(2L);
		Tag tag1=tagDAO.get(1L);
		Tag tag2=tagDAO.get(2L);
		Set<Tag> list= tagDAO.getTagsById(tags);
		assertEquals(true, list.contains(tag1));
		assertEquals(true, list.contains(tag2));
		assertEquals(2, list.size());
	}

}