package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.Filter;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.INewsService;

/**
 * The Class NewsLogic.
 */
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class NewsService implements INewsService {

	/** The dao. */
	private INewsDAO newsDAO;

	private static Logger logger = Logger
			.getLogger(NewsService.class.getName());

	public long add(News news) throws ServiceException {
		ServiceUtils.assertNotNullEntity(news, "news");
		try {
			return newsDAO.add(news);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (add news failed): " + e);
			throw new ServiceException(e);
		}
	}

	public void remove(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "news");
		try {
			newsDAO.remove(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (remove news failed): " + e);
			throw new ServiceException(e);
		}
	}

	public News get(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "news");
		try {
			return newsDAO.get(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get news failed): " + e);
			throw new ServiceException(e);
		}
	}

	public void update(News news) throws ServiceException {
		ServiceUtils.assertNotNullEntity(news, "news");
		try {
			newsDAO.update(news);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (update news failed): " + e);
			throw new ServiceException(e);
		}
	}

	public List<News> getAllNews() throws ServiceException {
		try {
			return newsDAO.getAllNews();
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get all news failed): " + e);
			throw new ServiceException(e);
		}
	}
	
	public long getCountOfFilterPage(Filter filter) throws ServiceException {

		long count = 0;
		try {
			count = newsDAO.getCountOfFilterPage(filter);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}

		return count;
	}


	public News getFilterNews(Filter filter, long rowId)
			throws ServiceException {
		News news = new News();
		try {

			news = newsDAO.getFilterNews(filter, rowId);

		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return news;
	}


	public List<News> getFilterList(Filter filter, long size, long page)
			throws ServiceException {
		List<News> news = new ArrayList<News>();
		try {
			news = newsDAO.getFilterList(filter, size, page);
		} catch (DAOException e) {
			throw new ServiceException(e);
		}
		return news;
	}

	public void setNewsDAO(INewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}

	public void remove(List<Long> deleteIdList) throws ServiceException {		
		try {
			newsDAO.remove(deleteIdList);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (remove news failed): " + e);
			throw new ServiceException(e);
		}

	}

	public INewsDAO getNewsDAO() {
		return newsDAO;
	}

}
