package com.epam.newsmanagement.entity.vo;

import java.util.HashSet;
import java.util.Set;

import com.epam.newsmanagement.entity.Tag;


public class TagVO {

	private Long tagId;
	private String name;
	private Set<NewsVO> news;
	
	public TagVO() {
		super();
		this.tagId = new Long(0);
		this.name = "";
		this.news = new HashSet<NewsVO>();
	}
	
	public TagVO(Long tagId, String name, Set<NewsVO> news) {
		super();
		this.tagId = tagId;
		this.name = name;
		this.news = news;
	}
	
	public TagVO(Tag tag) {
		super();
		this.tagId = tag.getId();
		this.name = tag.getName();
		this.news = new HashSet<NewsVO>();
		
	}
	
	public Long getTagId() {
		return tagId;
	}
	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Set<NewsVO> getNews() {
		return news;
	}
	public void setNews(Set<NewsVO> news) {
		this.news = news;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TagVO other = (TagVO) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (tagId == null) {
			if (other.tagId != null)
				return false;
		} else if (!tagId.equals(other.tagId))
			return false;
		return true;
	}

}
