package com.epam.newsmanagement.dao.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Class CommentDAO.
 */
@Transactional("transactionManager")
public class CommentDAO implements ICommentDAO {

	private EntityManager entityManager;

	public void add(Comment comment) throws DAOException {
		try {
			entityManager.persist(comment);
			entityManager.flush();
		} catch (Exception e) {
			throw new DAOException("Exception in add comment jpa" + e);
		}
	}

	public void remove(long id) throws DAOException {
		try {
			Comment comment = entityManager.find(Comment.class, id);
			comment.getNews().getComments().remove(comment);
			entityManager.flush();
			entityManager.remove(comment);
		} catch (Exception e) {
			throw new DAOException("Exception in remove comment jpa" + e);
		}

	}

	public Comment get(long id) throws DAOException {
		try {
			Comment result = entityManager.find(Comment.class, id);
			return result;
		} catch (Exception e) {
			throw new DAOException("Exception in get comment jpa" + e);
		}
	}

	public void update(Comment comment) throws DAOException {
		entityManager.merge(comment);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Comment> getAllByNewsId(long id) throws DAOException {
		try {
			List<Comment> comments = new ArrayList<Comment>();
			Query query = entityManager
					.createQuery("select a from Comments a where a.news.id=(:newsId)");
			query.setParameter("newsId", id);
			comments = query.getResultList();
			return comments;
		} catch (Exception e) {
			throw new DAOException("Exception in getAllByNewsId comment jpa" + e);
		}
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
}
