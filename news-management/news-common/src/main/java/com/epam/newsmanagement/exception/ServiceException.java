package com.epam.newsmanagement.exception;


/**
 * The Class TechnicalLogicException.
 */
@SuppressWarnings("serial")
public class ServiceException extends Exception {

    /**
     * Instantiates a new technical logic exception.
     *
     * @param message the message
     */
    public ServiceException(String message) {
        super(message);
    }

    /**
     * Instantiates a new technical logic exception.
     *
     * @param message the message
     * @param cause the cause
     */
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new technical logic exception.
     *
     * @param cause the cause
     */
    public ServiceException(Throwable cause) {
        super(cause);
    }
}
