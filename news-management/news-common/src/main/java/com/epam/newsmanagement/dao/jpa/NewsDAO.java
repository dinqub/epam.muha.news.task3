package com.epam.newsmanagement.dao.jpa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.Filter;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Class NewsDAO.
 */
@Transactional("transactionManager")
public class NewsDAO implements INewsDAO {

	private EntityManager entityManager;

	public long add(News news) throws DAOException {
		try {
			entityManager.persist(news);
			return news.getId();
		} catch (Exception e) {
			throw new DAOException("Exception in add news jpa" + e);
		}
	}

	public void remove(long id) throws DAOException {
		try {
			News news = entityManager.find(News.class, id);
			entityManager.remove(news);
		} catch (Exception e) {
			throw new DAOException("Exception in remove news jpa" + e);
		}
	}

	public News get(long id) throws DAOException {
		try {
			News news = entityManager.find(News.class, id);
			return news;
		} catch (Exception e) {
			throw new DAOException("Exception in get news jpa" + e);
		}
	}

	public void update(News news) throws DAOException {
		try {
			entityManager.merge(news);
		} catch (Exception e) {
			throw new DAOException("Exception in update news jpa" + e);
		}
	}

	public List<News> getAllNews() throws DAOException {
		try {
			List<News> news = new ArrayList<News>();
			CriteriaBuilder criteriaBuilder = entityManager
					.getCriteriaBuilder();
			CriteriaQuery<News> q = criteriaBuilder.createQuery(News.class);
			TypedQuery<News> query = entityManager.createQuery(q);
			news = query.getResultList();
			return news;
		} catch (Exception e) {
			throw new DAOException("Exception in getAllNews news jpa" + e);
		}
	}

	public List<News> getFilterList(Filter filter, long totalsize, long page)
			throws DAOException {
		try {
			List<News> result = new ArrayList<News>();
			List<Predicate> predicates = new ArrayList<Predicate>();
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<News> criteria = builder.createQuery(News.class);
			Root<News> newsRoot = criteria.from(News.class);
			if (filter.getTags() != null && filter.getTags().size() != 0) {
				Predicate predicate = newsRoot.join("tags").get("id")
						.in(filter.getTags());
				predicates.add(predicate);
			}
			if (filter.getAuthorId() != null && filter.getAuthorId() != 0) {
				Predicate predicate = builder.equal(newsRoot.join("author")
						.get("id"), filter.getAuthorId());
				predicates.add(predicate);
			}
			criteria.where(predicates.toArray(new Predicate[predicates.size()]));
			criteria.orderBy(builder.desc(builder.size(newsRoot
					.<Collection<Comment>> get("comments"))), builder
					.desc(newsRoot.get("modificationDate")));
			CriteriaQuery<News> select = criteria.select(newsRoot).distinct(
					true);
			TypedQuery<News> query = entityManager.createQuery(select);

			query.setFirstResult((int) (totalsize * (page - 1)));
			query.setMaxResults((int) (totalsize));
			query.setHint("eclipselink.batch", "a.author");
			query.setHint("eclipselink.batch", "a.tags");
			query.setHint("eclipselink.batch", "a.comments");
			query.setHint("eclipselink.BATCH_SIZE", "100");
			query.setHint("eclipselink.batch.type", "IN");

			result = query.getResultList();

			for (News news : result) {
				news.setCountComment(news.getComments().size());
			}

			return result;
		} catch (Exception e) {
			throw new DAOException("Exception in getFilterList news jpa" + e);
		}
	}

	public long getCountOfFilterPage(Filter filter) throws DAOException {
		try {
			long result = 0;
			List<Predicate> predicates = new ArrayList<Predicate>();

			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
			Root<News> newsRoot = criteria.from(News.class);
			if (filter.getTags() != null && filter.getTags().size() != 0) {
				Predicate predicate = newsRoot.join("tags").get("id")
						.in(filter.getTags());
				predicates.add(predicate);
			}
			if (filter.getAuthorId() != null && filter.getAuthorId() != 0) {
				Predicate predicate = builder.equal(newsRoot.join("author")
						.get("id"), filter.getAuthorId());
				predicates.add(predicate);
			}
			criteria.where(predicates.toArray(new Predicate[predicates.size()]));

			CriteriaQuery<Long> select = criteria.select(builder
					.count(newsRoot));
			TypedQuery<Long> query = entityManager.createQuery(select);

			result = query.getSingleResult();

			return result;
		} catch (Exception e) {
			throw new DAOException("Exception in getCountOfFilterPage news jpa" + e);
		}
	}

	public News getFilterNews(Filter filter, long id) throws DAOException {
		try {
			News news = new News();
			List<Predicate> predicates = new ArrayList<Predicate>();

			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			CriteriaQuery<News> criteria = builder.createQuery(News.class);
			Root<News> newsRoot = criteria.from(News.class);
			if (filter.getTags() != null && filter.getTags().size() != 0) {
				Predicate predicate = newsRoot.join("tags").get("id")
						.in(filter.getTags());
				predicates.add(predicate);
			}
			if (filter.getAuthorId() != null && filter.getAuthorId() != 0) {
				Predicate predicate = builder.equal(newsRoot.join("author")
						.get("id"), filter.getAuthorId());
				predicates.add(predicate);
			}

			criteria.where(predicates.toArray(new Predicate[predicates.size()]));
			criteria.orderBy(builder.desc(builder.size(newsRoot
					.<Collection<Comment>> get("comments"))), builder
					.desc(newsRoot.get("modificationDate")));
			CriteriaQuery<News> select = criteria.select(newsRoot).distinct(
					true);
			TypedQuery<News> query = entityManager.createQuery(select);
			query.setFirstResult((int) (id - 1));
			query.setMaxResults((int) (1));
			news = (News) query.getSingleResult();

			return news;
		} catch (Exception e) {
			throw new DAOException("Exception in getFilterNews news jpa" + e);
		}
	}

	public void remove(List<Long> deleteIdList) throws DAOException {
		try {
			Query query = entityManager
					.createQuery("DELETE FROM News c WHERE c.id in :p");
			query.setParameter("p", deleteIdList).executeUpdate();
		} catch (Exception e) {
			throw new DAOException("Exception in remove news jpa" + e);
		}
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

}
