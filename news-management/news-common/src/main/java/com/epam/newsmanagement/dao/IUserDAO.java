package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.User;

public interface IUserDAO {
	 
	User findByUserName(String username);
 
}