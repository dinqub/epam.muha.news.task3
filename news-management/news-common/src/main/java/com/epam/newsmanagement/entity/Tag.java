package com.epam.newsmanagement.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The Class Tag.
 */
@Entity(name="Tag")
@Table(name = "Tag")
public class Tag implements Serializable {

	private static final long serialVersionUID = 446589848289653937L;

	/** The id of Tag. */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAG_SEQ")
	@SequenceGenerator(name = "TAG_SEQ", sequenceName = "TAG_SEQ", allocationSize = 1)
	@Column(name = "TAG_ID")
	private Long id = new Long(0);

	/** The name of Tag. */
	@Column(name = "TAG_NAME")
	private String name= "";;

	/**
	 * Instantiates a new tag .
	 */
	public Tag() {
	}
	
	public Tag(Long id) {
		this.id = new Long(id);
	}

	/**
	 * Instantiates a new tag.
	 *
	 * @param id
	 *            of Tag
	 * @param name
	 *            of Tag
	 */
	public Tag(long id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * Instantiates a new tag.
	 *
	 * @param name
	 *            of Tag
	 */
	public Tag(String name) {
		this.id=new Long(0);
		this.name = name;
	}

	/**
	 * Gets the id of Tag
	 *
	 * @return the id of Tag
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id of Tag
	 *
	 * @param id
	 *            of Tag
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the name of Tag
	 *
	 * @return the name of Tag
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of Tag
	 *
	 * @param name
	 *            of Tag
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Tag [id=" + id + ", name=" + name + "]";
	
	}

	
}
