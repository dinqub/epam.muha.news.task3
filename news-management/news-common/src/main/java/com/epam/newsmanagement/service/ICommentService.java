package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;


/**
 * The Interface ICommentLogic.
 */
public interface ICommentService {

	/**
	 * Add new comment to news
	 *
	 * @param comment - new comment
	 * @throws ServiceException the technical logic exception
	 */
	public void add(Comment comment) throws ServiceException;

	/**
	 * Removes comment from news
	 *
	 * @param id - id of removing comment
	 * @throws ServiceException the technical logic exception
	 */
	public void remove(long id) throws ServiceException;
	
	/**
	 * Gets comment by id
	 *
	 * @param id - id of comment
	 * @return  comment by id
	 * @throws ServiceException the technical logic exception
	 */
	public Comment get(long id) throws ServiceException;
	
	
	/**
	 * Update comment 
	 *
	 * @param comment - new comment
	 * @throws ServiceException the technical logic exception
	 */
	public void update(Comment comment) throws ServiceException;

	/**
	 * Sets the dao.
	 *
	 * @param mockedCommentDAO the new dao
	 */
	public void setCommentDAO(ICommentDAO mockedCommentDAO);
}
