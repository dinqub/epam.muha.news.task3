package com.epam.newsmanagement.exception;

/**
 * The Class TechnicalDAOException.
 */
@SuppressWarnings("serial")
public class DAOException extends Exception {


    /**
     * Instantiates a new technical dao exception.
     *
     * @param message the message
     */
    public DAOException(String message) {
        super(message);
    }

    /**
     * Instantiates a new technical dao exception.
     *
     * @param message the message
     * @param cause the cause
     */
    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new technical dao exception.
     *
     * @param cause the cause
     */
    public DAOException(Throwable cause) {
        super(cause);
    }
}
