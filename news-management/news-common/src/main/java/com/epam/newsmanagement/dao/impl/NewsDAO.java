package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.Filter;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Class NewsDAO.
 */
@Repository("newsDAO")
public class NewsDAO implements INewsDAO {

	/** The Constant SQL_INSRERT_NEWS. */
	public static final String SQL_INSERT_NEWS = "INSERT INTO news (news_id,short_text,full_text,title,creation_date,modification_date) VALUES(news_seq.nextval,?,?,?,?,?)";

	/** The Constant SQL_DELETE_NEWS. */
	public static final String SQL_DELETE_NEWS = "DELETE FROM news WHERE news_id=(?)";

	/** The Constant SQL_SELECT_ALL_NEWS. */

	public static final String SQL_SELECT_ALL_NEWS = "SELECT news.news_id,short_text,full_text,title,news.creation_date,modification_date FROM news ORDER BY (SELECT -COUNT(comments.NEWS_ID) AS countid FROM COMMENTS WHERE comments.news_id=news.news_id)";
	/** The Constant SQL_SELECT_NEWS. */
	public static final String SQL_SELECT_NEWS = "SELECT short_text,full_text,title,creation_date,modification_date FROM news WHERE news_id=(?)";

	/** The Constant SQL_SELECT_NEWS_BY_AUTHOR. */
	public static final String SQL_SELECT_NEWS_BY_AUTHOR = "SELECT news.news_id,short_text,full_text,title,creation_date,modification_date FROM (news INNER JOIN news_author ON news.news_id=news_author.news_id)  WHERE news_author.author_id=(?)";

	/** The Constant SQL_SELECT_NEWS_BY_TAG_BEGIN. */
	public static final String SQL_SELECT_NEWS_BY_TAG_BEGIN = "SELECT news.news_id,short_text,full_text,title,creation_date,modification_date FROM (news INNER JOIN news_tag ON news.news_id=news_tag.news_id)  WHERE news_tag.tag_id IN";

	/** The Constant SQL_SELECT_NEWS_BY_TAG_END. */
	public static final String SQL_SELECT_NEWS_BY_TAG_END = " GROUP BY (news.NEWS_ID,short_text,full_text,title,creation_date,modification_date)";

	/** The Constant SQL_UPDATE_NEWS. */
	public static final String SQL_UPDATE_NEWS = "UPDATE news SET short_text=?,full_text=?,title=?,modification_date=? WHERE news_id=?";

	public static final String SQL_DELETE_BY_LIST_NEWS="DELETE FROM news WHERE news_id IN (";
	
	/** The data source. */
	
	public static final String SQL_SELECT_NEWS_FIRST = "WITH filtertag as (SELECT news.news_id,short_text,full_text,title,creation_date,modification_date FROM (news LEFT JOIN news_tag ON news.news_id=news_tag.news_id) ";
	public static final String SQL_SELECT_NEWS_SECOND = "GROUP BY (news.NEWS_ID,short_text,full_text,title,creation_date,modification_date)),     filterauthor as(SELECT filtertag.news_id,short_text,full_text,title,creation_date,modification_date FROM (filtertag INNER JOIN news_author ON filtertag.news_id=news_author.news_id)";
	public static final String SQL_SELECT_NEWS_THRID = "GROUP BY (filtertag.NEWS_ID,short_text,full_text,title,creation_date,modification_date)),resultset as(SELECT filterauthor.news_id,short_text,full_text,title,filterauthor.creation_date,modification_date FROM filterauthor ORDER BY (SELECT -COUNT(comments.NEWS_ID) AS countid FROM COMMENTS WHERE comments.news_id=filterauthor.news_id)ASC,filterauthor.modification_date DESC) SELECT news_id,short_text,full_text,title,creation_date,modification_date FROM (SELECT news_id,short_text,full_text,title,creation_date,modification_date,ROWNUM RNUM FROM resultset WHERE ROWNUM <=?) WHERE rnum > ?";
	public static final String SQL_SELECT_NEWS_COUNT = "GROUP BY (filtertag.NEWS_ID,short_text,full_text,title,creation_date,modification_date)),resultset as(SELECT filterauthor.news_id,short_text,full_text,title,filterauthor.creation_date,modification_date FROM filterauthor ORDER BY (SELECT -COUNT(comments.NEWS_ID) AS countid FROM COMMENTS WHERE comments.news_id=filterauthor.news_id),filterauthor.title DESC) SELECT COUNT(*) as rowCount  FROM resultset";

	@Autowired
	private DataSource dataSource;

	public long add(News news) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		long newsId = 0;

		try {
			connection = DAOUtils.getConnection(dataSource);
			String generatedColumns[] = { "NEWS_ID" };
			statement = connection.prepareStatement(SQL_INSERT_NEWS,
					generatedColumns);

			statement.setString(1, news.getShortText());
			statement.setString(2, news.getFullText());
			statement.setString(3, news.getTitle());
			statement.setTimestamp(4, new Timestamp(news.getCreationDate()
					.getTime()));
			statement.setDate(5, new java.sql.Date(news.getModificationDate()
					.getTime()));
			statement.execute();

			resultSet = statement.getGeneratedKeys();
			if (resultSet.next()) {
				newsId = resultSet.getLong(1);
			} else {
				throw new SQLException("Creating news failed, no ID obtained.");
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.close(resultSet, statement, connection, dataSource);
		}
		return newsId;
	}

	public void remove(long id) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_DELETE_NEWS);
			statement.setLong(1, id);

			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.close(null, statement, connection, dataSource);
		}
	}

	public News get(long id) throws DAOException {
		News news = new News();
		news.setId(id);
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_SELECT_NEWS);
			statement.setLong(1, id);

			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				news.setShortText(resultSet.getString("short_text"));
				news.setFullText(resultSet.getString("full_text"));
				news.setTitle(resultSet.getString("title"));
				news.setCreationDate(resultSet.getTimestamp("creation_date"));
				news.setModificationDate(resultSet.getDate("modification_date"));
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.close(resultSet, statement, connection, dataSource);
		}
		return news;
	}

	public void update(News news) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		long newsId = news.getId();
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_UPDATE_NEWS);

			statement.setString(1, news.getShortText());
			statement.setString(2, news.getFullText());
			statement.setString(3, news.getTitle());
			statement.setDate(4, new java.sql.Date(news.getModificationDate()
					.getTime()));
			statement.setLong(5, newsId);

			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.close(null, statement, connection, dataSource);
		}

	}

	public List<News> getAllNews() throws DAOException {
		List<News> result = new ArrayList<News>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_SELECT_ALL_NEWS);
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				News news = buildNews(resultSet);
				result.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.close(resultSet, statement, connection, dataSource);
		}

		return result;
	}

	public List<News> searchByAuthor(long authorId) throws DAOException {
		List<News> newsShell = new ArrayList<News>();
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(SQL_SELECT_NEWS_BY_AUTHOR);
			statement.setLong(1, authorId);

			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				News news = buildNews(resultSet);
				newsShell.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.close(resultSet, statement, connection, dataSource);
		}

		return newsShell;
	}

	public List<News> searchByTags(List<Tag> tags) throws DAOException {
		List<News> newsShell = new ArrayList<News>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		StringBuilder query = new StringBuilder(SQL_SELECT_NEWS_BY_TAG_BEGIN);
		query.append(" (");

		int size = tags.size();

		for (int i = 0; i < size; ++i) {
			if (i < size - 1)
				query.append(tags.get(i).getId() + ",");
			else
				query.append(tags.get(i).getId());
		}
		query.append(")");
		query.append(SQL_SELECT_NEWS_BY_TAG_END);

		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(query.toString());
			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				News news = buildNews(resultSet);
				newsShell.add(news);
			}
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.close(resultSet, statement, connection, dataSource);
		}

		return newsShell;
	}

	public List<News> getFilterList(Filter filter, long totalsize, long page)
			throws DAOException {
		List<News> result = new ArrayList<News>();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		StringBuilder query = DAOUtils.sqlBuilder(filter,
				SQL_SELECT_NEWS_FIRST, SQL_SELECT_NEWS_SECOND,
				SQL_SELECT_NEWS_THRID);

		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(query.toString());
			statement.setLong(1, totalsize * page);
			statement.setLong(2, totalsize * (page - 1));

			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				News news = buildNews(resultSet);
				result.add(news);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.close(resultSet, statement, connection, dataSource);
		}

		return result;
	}

	public long getCountOfFilterPage(Filter filter) throws DAOException {
		long result = 0;
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		StringBuilder query = DAOUtils.sqlBuilder(filter,
				SQL_SELECT_NEWS_FIRST, SQL_SELECT_NEWS_SECOND,
				SQL_SELECT_NEWS_COUNT);

		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(query.toString());

			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				result = resultSet.getLong("rowCount");
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.close(resultSet, statement, connection, dataSource);
		}

		return result;
	}

	public News getFilterNews(Filter filter, long id) throws DAOException {
		News news = new News();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;

		StringBuilder query = DAOUtils.sqlBuilder(filter,
				SQL_SELECT_NEWS_FIRST, SQL_SELECT_NEWS_SECOND,
				SQL_SELECT_NEWS_THRID);

		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(query.toString());
			statement.setLong(1, id);
			statement.setLong(2, id - 1);

			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				news = buildNews(resultSet);
			}

		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.close(resultSet, statement, connection, dataSource);
		}

		return news;
	}
	public void remove(List<Long> deleteIdList) throws DAOException {
		Connection connection = null;
		PreparedStatement statement = null;
		StringBuilder query =DAOUtils.sqlDeleteBuilder(SQL_DELETE_BY_LIST_NEWS,deleteIdList);
		
		try {
			connection = DAOUtils.getConnection(dataSource);
			statement = connection.prepareStatement(query.toString());

			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(e);
		} finally {
			DAOUtils.close(null, statement, connection, dataSource);
		}

	}

	private News buildNews(ResultSet resultSet) throws SQLException {
		News news = new News();
		news.setId(resultSet.getLong("news_id"));
		news.setShortText(resultSet.getString("short_text"));
		news.setFullText(resultSet.getString("full_text"));
		news.setTitle(resultSet.getString("title"));
		news.setCreationDate(resultSet.getTimestamp("creation_date"));
		news.setModificationDate(resultSet.getDate("modification_date"));
		return news;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public DataSource getDataSource() {
		return dataSource;
	}
}
