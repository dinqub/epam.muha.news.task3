package com.epam.newsmanagement.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.Filter;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Class NewsDAO.
 */
@Transactional("transactionManager")
public class NewsDAO implements INewsDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public long add(News news) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.save(news);
			return news.getId();
		} catch (Exception e) {
			throw new DAOException("Exception in add news hibernate" + e);
		}
	}

	public void remove(long id) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			News news = new News();
			news.setId(id);
			session.delete(news);
		} catch (Exception e) {
			throw new DAOException("Exception in remove news hibernate" + e);
		}
	}

	public News get(long id) throws DAOException {
		try {
			News news = new News();
			Session session = sessionFactory.getCurrentSession();
			news = (News) session.get(News.class, id);
			Hibernate.initialize(news);
			return news;
		} catch (Exception e) {
			throw new DAOException("Exception in get news hibernate" + e);
		}
	}

	public void update(News news) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.update(news);
			session.flush();
		} catch (Exception e) {
			throw new DAOException("Exception in update news hibernate" + e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<News> getAllNews() throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			Criteria crit = session.createCriteria(News.class);
			crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			List<News> news = crit.list();
			return news;
		} catch (Exception e) {
			throw new DAOException("Exception in getAllNews news hibernate" + e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<News> getFilterList(Filter filter, long totalsize, long page)
			throws DAOException {
		try {
			List<News> result = new ArrayList<News>();

			Session session = sessionFactory.getCurrentSession();
			Criteria crit = session.createCriteria(News.class);

			if (filter.getTags() != null && filter.getTags().size() != 0) {
				crit.createAlias("tags", "tag", JoinType.LEFT_OUTER_JOIN);
				crit.add(Restrictions.in("tag.id", filter.getTags()));
			}
			if (filter.getAuthorId() != null && filter.getAuthorId() != 0) {
				crit.createAlias("author", "author", JoinType.LEFT_OUTER_JOIN);
				crit.add(Restrictions.eq("author.id", filter.getAuthorId()));
			}

			crit.addOrder(Order.desc("countComment"));
			crit.addOrder(Order.desc("modificationDate"));
			crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			crit.setFirstResult((int) (totalsize * (page - 1)));
			crit.setMaxResults((int) (totalsize));
			result = crit.list();

			return result;
		} catch (Exception e) {
			throw new DAOException("Exception in getFilterList news hibernate" + e);
		}
	}

	public long getCountOfFilterPage(Filter filter) throws DAOException {
		try {
			long result = 0;
			Session session = sessionFactory.getCurrentSession();

			Criteria crit = session.createCriteria(News.class);

			if (filter.getTags() != null && filter.getTags().size() != 0) {
				crit.createAlias("tags", "tag");
				crit.add(Restrictions.in("tag.id", filter.getTags()));
			}
			if (filter.getAuthorId() != null && filter.getAuthorId() != 0) {
				crit.createAlias("author", "author", JoinType.LEFT_OUTER_JOIN);
				crit.add(Restrictions.eq("author.id", filter.getAuthorId()));
			}
			crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			crit.setProjection(Projections.rowCount());
			result = (long) crit.uniqueResult();
			;

			return result;
		} catch (Exception e) {
			throw new DAOException("Exception in getCountOfFilterPage news hibernate" + e);
		}
	}

	public News getFilterNews(Filter filter, long id) throws DAOException {
		try {
			News news = new News();
			Session session = sessionFactory.getCurrentSession();
			Criteria crit = session.createCriteria(News.class);

			if (filter.getTags() != null && filter.getTags().size() != 0) {
				crit.createAlias("tags", "tag");
				crit.add(Restrictions.in("tag.id", filter.getTags()));
			}
			if (filter.getAuthorId() != null && filter.getAuthorId() != 0) {
				crit.createAlias("author", "author", JoinType.LEFT_OUTER_JOIN);
				crit.add(Restrictions.eq("author.id", filter.getAuthorId()));
			}

			crit.addOrder(Order.desc("countComment"));
			crit.addOrder(Order.asc("modificationDate"));
			crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			crit.setFirstResult((int) (id - 1));
			crit.setMaxResults((int) (1));
			news = (News) crit.uniqueResult();

			return news;
		} catch (Exception e) {
			throw new DAOException("Exception in getFilterNews news hibernate" + e);
		}
	}

	public void remove(List<Long> deleteIdList) throws DAOException {
		try {

			Session session = sessionFactory.getCurrentSession();

			Query query = session
					.createQuery("delete from News where (id in (:deletList))");
			query.setParameterList("deletList", deleteIdList);
			query.executeUpdate();
		} catch (Exception e) {
			throw new DAOException("Exception in remove news hibernate" + e);
		}
	}



}
