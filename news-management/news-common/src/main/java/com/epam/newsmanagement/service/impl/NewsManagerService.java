package com.epam.newsmanagement.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Filter;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.service.INewsManagerService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;

@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class NewsManagerService implements INewsManagerService {

	/** The author logic. */
	private IAuthorService authorService;

	/** The comment logic. */
	private ICommentService commentService;

	/** The tag logic. */
	private ITagService tagService;

	/** The news logic. */
	private INewsService newsService;

	public void addNews(News news, List<Long> tagsId, Long authorId)
			throws ServiceException {
		Set<Tag> tags = tagService.getTagsById(tagsId);
		news.addTags(tags);
		Author author = authorService.get(authorId);
		news.addAuthor(author);
		newsService.add(news);
	}

	@Override
	public void updateNews(News news, List<Long> tagIdList, Long authorId)
			throws ServiceException {
		News newsUp = newsService.get(news.getId());
		if (newsUp.getVersionId().equals(news.getVersionId())) {
			Set<Tag> tags = tagService.getTagsById(tagIdList);
			newsUp.addTags(tags);
			if (authorId != null) {
				Author author = authorService.get(authorId);
				newsUp.addAuthor(author);
			}
			newsUp.setInformation(news);
			newsService.update(newsUp);
		}
		else{
			throw new ServiceException("The news was been update,try update news again");
		}
	}

	public void removeNews(List<Long> deleteIdList) throws ServiceException {
		for (Long id : deleteIdList) {
			ServiceUtils.assertIsExistingId(id, "news");
		}
		newsService.remove(deleteIdList);

	}

	public News getNews(long id) throws ServiceException {
		News news = newsService.get(id);
		if (news == null)
			throw new ServiceException("News with calling id doesn't exist!");
		return news;
	}

	public List<News> getSortedNews() throws ServiceException {

		List<News> news = newsService.getAllNews();

		return news;
	}

	public void setLogic(ITagService tag, ICommentService comment,
			INewsService news, IAuthorService author) {
		this.authorService = author;
		this.commentService = comment;
		this.newsService = news;
		this.tagService = tag;
	}

	@Override
	public List<News> getFilterNewsList(Filter filter, long page,
			long countOfPage) throws ServiceException {
		ServiceUtils.assertIsExistingId(page, "page");
		List<News> news = newsService.getFilterList(filter, countOfPage, page);
		return news;
	}

	@Override
	public long getCountOfFilterPage(Filter filter) throws ServiceException {
		return newsService.getCountOfFilterPage(filter);
	}

	public News getFilterNews(Filter filter, long rowId)
			throws ServiceException {
		return newsService.getFilterNews(filter, rowId);
	}

	public List<Tag> getAllTags() throws ServiceException {

		return tagService.getAll();
	}

	public List<Author> getAllAuthors() throws ServiceException {

		return authorService.getAll();
	}

	@Override
	public Author getAuthorById(long authorId) throws ServiceException {

		return authorService.get(authorId);
	}

	@Override
	public Tag getTagById(Long tag) throws ServiceException {

		return tagService.get(tag);
	}

	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}

	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}

	public void setTagService(ITagService tagService) {
		this.tagService = tagService;
	}

	public INewsService getNewsService() {
		return newsService;
	}

	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}

	public IAuthorService getAuthorService() {
		return authorService;
	}

	public ICommentService getCommentService() {
		return commentService;
	}

	public ITagService getTagService() {
		return tagService;
	}

}
