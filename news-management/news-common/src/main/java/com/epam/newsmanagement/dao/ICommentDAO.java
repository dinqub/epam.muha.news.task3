package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Interface ICommentDAO.
 */
public interface ICommentDAO {

	/**
	 * Adds new comment of news in database.
	 *
	 * @param comment - new comment
	 * @throws DAOException the technical dao exception
	 */
	public void add(Comment comment) throws DAOException;

	/**
	 * Removes comment of news from database.
	 *
	 * @param id - id of comment
	 * @throws DAOException the technical dao exception
	 */
	public void remove(long id) throws DAOException;
	
	/**
	 * Gets the comment from database by id.
	 *
	 * @param id - id of comment
	 * @return comment by id
	 * @throws DAOException the technical dao exception
	 */
	public Comment get(long id)throws DAOException ;	
	
	/**
	 * Gets the comment from database by news id.
	 *
	 * @param id - id of news
	 * @return comment by id
	 * @throws DAOException the technical dao exception
	 */
	public List<Comment> getAllByNewsId(long id)throws DAOException ;	
		
	/**
	 * Update comment in database by id.
	 *
	 * @param comment - new comment
	 * @throws DAOException the technical dao exception
	 */
	public void update(Comment comment) throws DAOException;

}
