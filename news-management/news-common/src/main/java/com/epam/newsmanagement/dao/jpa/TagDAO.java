package com.epam.newsmanagement.dao.jpa;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Class TagDAO.
 */
@Transactional("transactionManager")
public class TagDAO implements ITagDAO {

	private EntityManager entityManager;

	public void add(Tag tag) throws DAOException {
		try {
			entityManager.persist(tag);
		} catch (Exception e) {
			throw new DAOException("Exception in add tag jpa" + e);
		}
	}

	public void remove(long id) throws DAOException {
		try {
			Tag tag = entityManager.find(Tag.class, id);
			entityManager.remove(tag);
		} catch (Exception e) {
			throw new DAOException("Exception in remove tag jpa" + e);
		}
	}

	public Tag get(long id) throws DAOException {
		try {
			Tag tag = entityManager.find(Tag.class, id);
			return tag;
		} catch (Exception e) {
			throw new DAOException("Exception in get tag jpa" + e);
		}
	}

	public void update(Tag tag) throws DAOException {
		try {
			entityManager.merge(tag);
		} catch (Exception e) {
			throw new DAOException("Exception in update tag jpa" + e);
		}
	}

	public List<Tag> getAll() throws DAOException {
		try {
			CriteriaBuilder criteriaBuilder = entityManager
					.getCriteriaBuilder();
			CriteriaQuery<Tag> q = criteriaBuilder.createQuery(Tag.class);
			TypedQuery<Tag> query = entityManager.createQuery(q);
			List<Tag> tag = query.getResultList();
			return tag;
		} catch (Exception e) {
			throw new DAOException("Exception in getAll tag jpa" + e);
		}
	}

	public void remove(List<Long> deleteIdList) throws DAOException {

	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Tag> getTagsById(List<Long> tagIdList) throws DAOException {
		try {
			Query query = entityManager
					.createQuery("select DISTINCT a from Tag a where a.id in :tagList");
			query.setParameter("tagList", tagIdList);

			List<Tag> list = query.getResultList();
			Set<Tag> set = new HashSet<Tag>(list);
			return set;
		} catch (Exception e) {
			throw new DAOException("Exception in getTagsById tag jpa" + e);
		}
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
}
