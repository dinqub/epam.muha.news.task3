package com.epam.newsmanagement.service.impl;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ITagService;

/**
 * The Class TagLogic.
 */
//@Service("tagService")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class TagService implements ITagService {

	/** The dao. */
	private ITagDAO tagDAO;

	/**
	 * Logger
	 */
	private static Logger logger = Logger.getLogger(TagService.class.getName());

	public void add(Tag tag) throws ServiceException {
		ServiceUtils.assertNotNullEntity(tag, "tag");
		try {
			tagDAO.add(tag);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (add tag failed): " + e);
			throw new ServiceException(e);
		}
	}

	public void remove(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "tag");
		try {
			tagDAO.remove(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (remove tag failed): " + e);
			throw new ServiceException(e);
		}
	}

	public Tag get(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "tag");
		try {
			return tagDAO.get(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get tag failed): " + e);
			throw new ServiceException(e);
		}
	}


	public void update(Tag tag) throws ServiceException {
		ServiceUtils.assertNotNullEntity(tag, "tag");
		try {
			tagDAO.update(tag);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (update tag failed): " + e);
			throw new ServiceException(e);
		}

	}

	public List<Tag> getAll() throws ServiceException {
		try {
			List<Tag> tags = tagDAO.getAll();
			tags.sort(new Comparator<Tag>() {

				public int compare(Tag comment1, Tag comment2) {
					return comment1.getName().compareTo(comment2.getName());
				}
			});
			return tags;
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get all tag failed): " + e);
			throw new ServiceException(e);
		}
	}

	
	public Set<Tag> getTagsById(List<Long> tagIdList) throws ServiceException {
		Set<Tag> set = new HashSet<Tag>();
		if (tagIdList != null && tagIdList.size() != 0) {
			try {
				set = tagDAO.getTagsById(tagIdList);
			} catch (DAOException e) {
				throw new ServiceException(e);
			}
		}
		return set;
	}

	public void setTagDAO(ITagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}

	public ITagDAO getTagDAO() {
		return tagDAO;
	}

}
