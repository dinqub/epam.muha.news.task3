package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.entity.Filter;

/**
 * The Class UtilitDAO.
 */
public class DAOUtils {

	private static Logger logger = Logger.getLogger(DAOUtils.class.getName());

	public static Connection getConnection(DataSource dataSource) {

		Connection connection = DataSourceUtils.getConnection(dataSource);

		return connection;

	}

	/**
	 * Close ResultSet ,Statement and Connection
	 *
	 * @param resultSet
	 *            - resultSet
	 * @param statment
	 *            - statement
	 * @param connection
	 *            - connection
	 */
	public static void close(ResultSet resultSet, Statement statment,
			Connection connection, DataSource dataSource) {

		try {
			if (resultSet != null) {
				resultSet.close();
			}
		} catch (SQLException e) {
			logger.error("SQL exception (request or table failed): " + e);
		} finally {
			try {
				if (statment != null) {
					statment.close();
				}
			} catch (SQLException e) {
				logger.error("SQL exception (request or table failed): " + e);
			} finally {
				DataSourceUtils.releaseConnection(connection, dataSource);
			}
		}
	}

	public static StringBuilder sqlBuilder(Filter filter, String first,
			String second, String thrid) {
		List<Long> tags = filter.getTags();
		Long author = filter.getAuthorId();

		StringBuilder query = new StringBuilder(first);

		if (tags != null) {
			int size = tags.size();
			if (size != 0) {
				query.append(" WHERE news_tag.tag_id IN (");
				int i = 0;
				for (Long tag : tags) {
					if (i < size - 1)
						query.append(tag + ",");
					else
						query.append(tag);
					++i;
				}
				query.append(")");
			}
		}
		query.append(second);

		if (author != null) {
			if (author != 0) {
				query.append(" WHERE news_author.author_id = " + author);
			}
		}
		query.append(thrid);
		return query;

	}
	
	public static StringBuilder sqlDeleteBuilder(String first,List<Long> list) {
		
		StringBuilder query = new StringBuilder(first);

		if (list != null) {
			int size = list.size();
			if (size != 0) {
				int i = 0;
				for (Long newsId : list) {
					if (i < size - 1)
						query.append(newsId + ",");
					else
						query.append(newsId);
					++i;
				}
				query.append(")");
			}
		}
		return query;

	}
	

}
