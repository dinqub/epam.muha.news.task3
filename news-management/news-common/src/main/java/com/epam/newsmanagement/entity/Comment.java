package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The Class Comment.
 */
@Entity(name="Comments")
@Table(name="Comments")
@Cacheable(false)
public class Comment implements Serializable {

	private static final long serialVersionUID = -4505806203297818467L;

	/** The id. */
	@Id
	@Column(name="comment_id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="COMMENT_SEQ") 
    @SequenceGenerator(name="COMMENT_SEQ",sequenceName="COMMENT_SEQ", allocationSize=1) 
	private Long id=new Long(0);

	/** The comment text. */
	@Column(name="comment_text")
	private String commentText="";

	/** The creation date. */
	@Column(name="creation_date")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date creationDate=new Date();

	@ManyToOne( fetch = FetchType.EAGER)
	@JoinColumn(name="news_id")
	private News news=new News();

	/**
	 * Instantiates a new comment.
	 */
	public Comment() {
	}

	/**
	 * Instantiates a new comment.
	 *
	 * @param id
	 *            of Comment
	 * @param commentText 
	 *            of Comment
	 * @param creationDate
	 *            of Comment
	 * @param newsId
	 *            of Comment
	 */
	public Comment(long id, String commentText, Date creationDate,News news) {
		super();
		this.id = id;
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.news = news;
	}
	public Comment( String commentText, Date creationDate,
			News news) {
		super();
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.news = news;
	}
	public Comment( String commentText, Long newsId) {
		super();
		this.commentText = commentText;
		this.creationDate = new Date();
		this.news = new News(newsId);
	}

	/**
	 * Instantiates a new comment.
	 *
	 * @param commentText
	 *            of Comment
	 * @param date
	 *            of Comment
	 */
	public Comment(String commentText, Date date) {
		this.commentText = commentText;
		this.creationDate = date;
	}
	public Comment(String commentText, News news) {
		this.commentText = commentText;
		this.news = news;
	}


	public Comment(long id) {
		this.id=id;
		this.creationDate =null;
	}

	/**
	 * Gets the id of Comment
	 *
	 * @return the id of Comment
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id of Comment
	 *
	 * @param id
	 *            of Comment
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the comment text of Comment
	 *
	 * @return the comment text of Comment
	 */
	public String getCommentText() {
		return commentText;
	}

	/**
	 * Sets the comment text of Comment
	 *
	 * @param commentText
	 *            of Comment
	 */
	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	/**
	 * Gets the creation date of Comment
	 *
	 * @return the creation date of Comment
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date of Comment
	 *
	 * @param creationDate
	 *            of Comment
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the news id of Comment
	 *
	 * @return the news id of Comment
	 */
	public Long  getNewsId() {
		return news.getId();
	}


	@Override
	public String toString() {
		return "Comment [id=" + id + ", commentText=" + commentText
				+ ", creationDate=" + creationDate + "]";
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
