package com.epam.newsmanagement.dao.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Class AuthorDAO.
 */
@Transactional("transactionManager")
public class AuthorDAO implements IAuthorDAO {

	private EntityManager entityManager;

	public void add(Author author) throws DAOException {
		try {
			entityManager.persist(author);
		} catch (Exception e) {
			throw new DAOException("Exception in add author jpa" + e);
		}
	}

	public void expired(long id) throws DAOException {
		try {
			Author author = (Author) entityManager.find(Author.class, id);
			Date expired = new Date();
			author.setExpired(expired);
			entityManager.merge(author);
		} catch (Exception e) {
			throw new DAOException("Exception in expired author jpa" + e);
		}
	}

	public void update(Author author) throws DAOException {
		try {
			entityManager.merge(author);
		} catch (Exception e) {
			throw new DAOException("Exception in add author jpa" + e);
		}
	}

	public Author get(long id) throws DAOException {
		try {
			Author author = (Author) entityManager.find(Author.class, id);
			return author;
		} catch (Exception e) {
			throw new DAOException("Exception in update author jpa" + e);
		}
	}

	public List<Author> getAll() throws DAOException {
		try {
			CriteriaBuilder criteriaBuilder = entityManager
					.getCriteriaBuilder();
			CriteriaQuery<Author> q = criteriaBuilder.createQuery(Author.class);
			TypedQuery<Author> query = entityManager.createQuery(q);
			List<Author> author = query.getResultList();
			return author;
		} catch (Exception e) {
			throw new DAOException("Exception in getAll author jpa" + e);
		}
	}

	// TODO
	public List<Author> getAllCurrentAuthor() throws DAOException {
		try {
			CriteriaBuilder criteriaBuilder = entityManager
					.getCriteriaBuilder();
			CriteriaQuery<Author> q = criteriaBuilder.createQuery(Author.class);
			TypedQuery<Author> query = entityManager.createQuery(q);
			List<Author> author = query.getResultList();
			return author;
		} catch (Exception e) {
			throw new DAOException("Exception in getAllCurrentAuthor author jpa" + e);
		}
	}

	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

}
