package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Filter;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * The Interface IMainLogic.
 */
public interface INewsManagerService {		
	/**
	 * Add the news.
	 *
	 * @param news the news shell
	 * @throws ServiceException the technical logic exception
	 */
	public void addNews(News news,List<Long> tag,Long authorId) throws  ServiceException;

	/**
	 * Remove news by news id
	 *
	 * @param deleteIdList - id of news
	 * @throws ServiceException the technical logic exception
	 */
	public void removeNews(List<Long> deleteIdList) throws ServiceException ;

	/**
	 * Get the news by id
	 *
	 * @param id - id of news
	 * @return the news by id
	 * @throws ServiceException the technical logic exception
	 */
	public News getNews(long id)throws ServiceException ;

	/**
	 * Update news by id
	 *
	 * @param news - new news
	 * @throws ServiceException the technical logic exception
	 */
	public void updateNews(News news, List<Long> tagIdList, Long authorId) throws ServiceException;

	/**
	 * Get the sorted list of news.
	 *
	 * @return the sorted list of news
	 * @throws ServiceException the technical logic exception
	 */
	public List<News> getSortedNews() throws ServiceException ;
	
	/**
	 * Sets the logic.
	 *
	 * @param tag - tag logic
	 * @param comment - comment logic
	 * @param news - news logic
	 * @param author - author logic
	 */
	public void setLogic(ITagService tag,ICommentService comment,INewsService news,IAuthorService author);

	public List<Tag> getAllTags() throws ServiceException;

	public  List<Author> getAllAuthors() throws ServiceException;

	public Author getAuthorById(long authorId) throws ServiceException;

	public Tag getTagById(Long tag) throws ServiceException;

	/**
	 * Gets the filter page.
	 *
	 * @param filter - filter of page 
	 * @param page - number of page
	 * @return the filter page
	 * @throws DAOException the DAO exception
	 * @throws ServiceException 
	 */
	public List<News> getFilterNewsList(Filter filter,long page,long countOfPage) throws  ServiceException;
	
	public News getFilterNews(Filter filter,long rowId) throws  ServiceException;
 	
	public long getCountOfFilterPage(Filter filter)throws  ServiceException;




}

