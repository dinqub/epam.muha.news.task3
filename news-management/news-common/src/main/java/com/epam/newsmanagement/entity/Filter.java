package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Filter implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2560360346836494281L;
	public Long authorId;
	public List<Long> tags ;

	public Filter(Long authorId, List<Long> tags) {
		super();
		this.authorId = authorId;
		this.tags = tags;
	}
	public Filter() {
		super();
		this.authorId = 0L;
		this.tags = new ArrayList<Long>();
	}

	public Long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
	public List<Long> getTags() {
		return tags;
	}
	public void setTags(List<Long> tags) {
		this.tags = tags;
	} 

	


}
