package com.epam.newsmanagement.dao.hibernate;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Class TagDAO.
 */

@Transactional("transactionManager")
public class TagDAO implements ITagDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void add(Tag tag) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.save(tag);
		} catch (Exception e) {
			throw new DAOException("Exception in add tag hibernate" + e);
		}
	}

	public void remove(long id) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			Tag tag = new Tag();
			tag.setId(id);
			session.delete(tag);
		} catch (Exception e) {
			throw new DAOException("Exception in remove tag hibernate" + e);
		}
	}

	public Tag get(long id) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			Tag result = null;
			result = (Tag) session.get(Tag.class, id);
			return result;
		} catch (Exception e) {
			throw new DAOException("Exception in get tag hibernate" + e);
		}
	}

	public void update(Tag tag) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.saveOrUpdate(tag);
		} catch (Exception e) {
			throw new DAOException("Exception in update tag hibernate" + e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Tag> getAll() throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			Criteria crit = session.createCriteria(Tag.class);
			List<Tag> tag = crit.list();
			return tag;
		} catch (Exception e) {
			throw new DAOException("Exception in getAll tag hibernate" + e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<Tag> getTagsById(List<Long> tagIdList) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();

			Criteria crit = session.createCriteria(Tag.class);
			crit.add(Restrictions.in("id", tagIdList));

			List<Tag> list = crit.list();
			Set<Tag> set = new HashSet<Tag>(list);
			return set;
		} catch (Exception e) {
			throw new DAOException("Exception in getTagsById tag hibernate" + e);
		}
	}


}
