package com.epam.newsmanagement.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Class AuthorDAO.
 */
@Transactional("transactionManager")
public class AuthorDAO implements IAuthorDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void add(Author author) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.save(author);
		} catch (Exception e) {
			throw new DAOException("Exception in add author hibernate" + e);
		}
	}

	public void expired(long id) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			Author author = (Author) session.get(Author.class, id);
			Date expired = new Date();
			author.setExpired(expired);
			session.saveOrUpdate(author);
		} catch (Exception e) {
			throw new DAOException("Exception in expired author hibernate" + e);
		}
	}

	public void update(Author author) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.update(author);
		} catch (Exception e) {
			throw new DAOException("Exception in update author hibernate" + e);
		}
	}

	public Author get(long id) throws DAOException {
		try {
			Author author = new Author();
			Session session = sessionFactory.getCurrentSession();
			author = (Author) session.get(Author.class, id);
			return author;
		} catch (Exception e) {
			throw new DAOException("Exception in get author hibernate" + e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Author> getAll() throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			Criteria crit = session.createCriteria(Author.class);
			List<Author> author = crit.list();
			return author;
		} catch (Exception e) {
			throw new DAOException("Exception in getAll author hibernate" + e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Author> getAllCurrentAuthor() throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			Criteria crit = session.createCriteria(Author.class);
			crit.add(Restrictions.isNull("expired"));
			List<Author> author = crit.list();
			return author;
		} catch (Exception e) {
			throw new DAOException("Exception in getAllCurrentAuthor author hibernate" + e);
		}
	}



}
