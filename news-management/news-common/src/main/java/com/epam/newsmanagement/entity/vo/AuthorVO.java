package com.epam.newsmanagement.entity.vo;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.epam.newsmanagement.entity.Author;

public class AuthorVO {

	private long id;
	
	private String name;

	private Date expired ;

	private Set<NewsVO> news;
	
	public AuthorVO() {
		super();
		this.id = new Long(0);
		this.name = "";
		this.expired = null;
		this.news = new HashSet<NewsVO>();
	}
	
	public AuthorVO(long id, String name, Date expired, Set<NewsVO> news) {
		super();
		this.id = id;
		this.name = name;
		this.expired = expired;
		this.news = news;
	}
	
	public AuthorVO(Author author) {
		super();
		this.id =author.getId();
		this.name = author.getName();
		this.expired = author.getExpired();
		this.news = new HashSet<NewsVO>();
		/*for (News news : author.getNews()) {
			NewsVO newsVO=new NewsVO(news);
			this.news.add(newsVO);
		}*/
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthorVO other = (AuthorVO) obj;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		return true;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

	public Set<NewsVO> getNews() {
		return news;
	}

	public void setNews(Set<NewsVO> news) {
		this.news = news;
	}
	
}
