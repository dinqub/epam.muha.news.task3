package com.epam.newsmanagement.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
 
@Entity
@Table(name = "Roles")
public class UserRole{
	
	@ManyToOne(fetch = FetchType.LAZY)	
	@JoinColumn(name="user_id")
	private User user=new User();
	
	@Id
	@Column(name = "role_name", nullable = false, length = 45)
	private String roleName;
 
	public UserRole() {
	}

	public UserRole(User user, String roleName) {
		super();
		this.user = user;
		this.roleName = roleName;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
 
 
 
	
}