package com.epam.newsmanagement.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

/**
 * The Class CommentDAO.
 */
@Transactional("transactionManager")
public class CommentDAO implements ICommentDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void add(Comment comment) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.save(comment);
		} catch (Exception e) {
			throw new DAOException("Exception in add comment hibernate" + e);
		}
	}

	public void remove(long id) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			Comment comment = (Comment) session.get(Comment.class, id);
			comment.getNews().getComments().remove(comment);
			session.flush();
			session.delete(comment);
		} catch (Exception e) {
			throw new DAOException("Exception in remove comment hibernate" + e);
		}
	}

	public Comment get(long id) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			Comment result = (Comment) session.get(Comment.class, id);
			return result;
		} catch (Exception e) {
			throw new DAOException("Exception in get comment hibernate" + e);
		}
	}

	public void update(Comment comment) throws DAOException {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.saveOrUpdate(comment);
		} catch (Exception e) {
			throw new DAOException("Exception in update comment hibernate" + e);
		}
	}


	@Override
	@SuppressWarnings("unchecked")
	public List<Comment> getAllByNewsId(long id) throws DAOException {
		try {
			List<Comment> comments = new ArrayList<Comment>();

			Session session = sessionFactory.getCurrentSession();
			Criteria crit = session.createCriteria(Comment.class);
			crit.add(Restrictions.eq("news.id", id));

			comments = crit.list();
			return comments;
		} catch (Exception e) {
			throw new DAOException("Exception in getAllByNewsId comment hibernate" + e);
		}
	}

}
