package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;



public interface IAuthorDAO {
	
	/**
	 * Adds the new Author in database.
	 *
	 * @param author new author
	 * @throws DAOException the technical dao exception
	 */
	public void add(Author author) throws DAOException;

	/**
	 * Fared author and record about this in database.
	 *
	 * @param id - id of author
	 * @throws DAOException the technical dao exception
	 */
	public void expired(long id) throws DAOException;

	/**
	 * Gets author by id from database.
	 *
	 * @param id - id of Author
	 * @return author - author of id
	 * @throws DAOException the technical dao exception
	 */
	public Author get(long id) throws DAOException;
	
	/**
	 * Gets the author from database by news id.
	 *
	 * @param newsId - news id
	 * @return author by news id
	 * @throws DAOException the technical dao exception
	 */
	
	/**
	 * Gets the all authors from database.
	 *
	 * @return list of all author
	 * @throws DAOException the technical dao exception
	 */
	public List<Author> getAll() throws DAOException;
	
	/**
	 * Update author in database.
	 *
	 * @param author - new author
	 * @throws DAOException the technical dao exception
	 */
	public void update(Author author) throws DAOException;

	public List<Author> getAllCurrentAuthor() throws DAOException ;

	
}
