package com.epam.newsmanagement.entity.vo;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;

public class NewsVO {
	
	private Long id;
	
	private String shortText;

	private String fullText;

	private String title;

	private Date creationDate;

	private Date modificationDate;

	private Set<TagVO> tags;

	private Set<CommentVO> comments;

	private Set<AuthorVO> author;
	public NewsVO() {
		super();
		this.id = new Long(0);
		this.shortText = "";
		this.fullText = "";
		this.title = "";
		this.creationDate = new Date();
		this.modificationDate = new Date();
		this.tags = new HashSet<TagVO>();
		this.comments = new HashSet<CommentVO>();
		this.author = new HashSet<AuthorVO>();
	}
	
	public NewsVO(Long id, String shortText, String fullText, String title,
			Date creationDate, Date modificationDate, Set<TagVO> tags,
			Set<CommentVO> comments, Set<AuthorVO> author) {
		super();
		this.id = id;
		this.shortText = shortText;
		this.fullText = fullText;
		this.title = title;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
		this.tags = tags;
		this.comments = comments;
		this.author = author;
	}

	public NewsVO(News news) {
		this.id = news.getId();
		this.shortText = news.getShortText();
		this.fullText = news.getFullText();
		this.title = news.getTitle();
		this.creationDate = news.getCreationDate();
		this.modificationDate = news.getModificationDate();
		for (Tag tag : news.getTags()) {
			TagVO tagVO=new TagVO(tag);
			this.tags.add(tagVO);
		}
		this.comments = new HashSet<CommentVO>();
		for (Comment comment : news.getComments()) {
			CommentVO commentVO=new CommentVO(comment);
			this.comments.add(commentVO);
		}
		AuthorVO authorVO=new AuthorVO((Author) Arrays.asList(news.getAuthor()).get(0));
		Set<AuthorVO> authorSet=new HashSet<AuthorVO>();
		authorSet.add(authorVO);
		this.author=authorSet;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Set<TagVO> getTags() {
		return tags;
	}

	public void setTags(Set<TagVO> tags) {
		this.tags = tags;
	}

	public Set<CommentVO> getComments() {
		return comments;
	}

	public void setComments(Set<CommentVO> comments) {
		this.comments = comments;
	}

	public Set<AuthorVO> getAuthor() {
		return author;
	}

	public void setAuthor(Set<AuthorVO> author) {
		this.author = author;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsVO other = (NewsVO) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsVO [id=" + id + ", shortText=" + shortText + ", fullText="
				+ fullText + ", title=" + title + ", creationDate="
				+ creationDate + ", modificationDate=" + modificationDate
				+ ", tags=" + tags + ", comments=" + comments + ", author="
				+ author + "]";
	}

}
