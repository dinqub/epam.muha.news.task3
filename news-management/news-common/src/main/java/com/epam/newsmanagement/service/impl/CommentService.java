package com.epam.newsmanagement.service.impl;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ICommentService;

/**
 * The Class CommentLogic.
 */
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CommentService implements ICommentService {

	/** The dao. */
	//@Autowired
	private ICommentDAO commentDAO;

	private static Logger logger = Logger.getLogger(CommentService.class
			.getName());

	public void add(Comment comment) throws ServiceException {
		ServiceUtils.assertNotNullEntity(comment, "comment");
		try {
			commentDAO.add(comment);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (add comment failed): " + e);
			throw new ServiceException(e);
		}

	}

	public void remove(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "comment");
		try {
			commentDAO.remove(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (remove comment failed): " + e);
			throw new ServiceException(e);
		}
	}

	public Comment get(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "comment");
		try {
			return commentDAO.get(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get comment failed): " + e);
			throw new ServiceException(e);
		}
	}

	public void update(Comment comment) throws ServiceException {
		ServiceUtils.assertNotNullEntity(comment, "comment");
		try {
			commentDAO.update(comment);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (update comment failed): " + e);
			throw new ServiceException(e);
		}
	}
	
	public void setCommentDAO(ICommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}

	public ICommentDAO getCommentDAO() {
		return commentDAO;
	}

}