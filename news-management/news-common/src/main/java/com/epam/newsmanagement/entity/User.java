package com.epam.newsmanagement.entity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name = "users")
public class User {

	@Id
	@Column(name = "user_id", nullable = false, length = 60)
	private Integer id;

	@Column(name = "username", unique = true, nullable = false, length = 45)
	private String username;

	@Column(name = "login", nullable = false, length = 60)
	private String login;

	@Column(name = "password", nullable = false, length = 60)
	private String password;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private Set<UserRole> userRole = new HashSet<UserRole>(0);

	public User() {
	}

	public User(Integer id, String username, String login, String password,
			Set<UserRole> userRole) {
		super();
		this.id = id;
		this.username = username;
		this.login = login;
		this.password = password;
		this.userRole = userRole;
	}

	public User(String username2, String password2, boolean enabled2,
			boolean b, boolean c, boolean d, List<GrantedAuthority> authorities) {
		// TODO Auto-generated constructor stub
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<UserRole> getUserRole() {
		return this.userRole;
	}

	public void setUserRole(Set<UserRole> userRole) {
		this.userRole = userRole;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

}