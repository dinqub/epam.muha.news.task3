package com.epam.newsmanagement.service.impl;

import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IAuthorService;

/**
 * The Class AuthorLogic.
 */
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class AuthorService implements IAuthorService {

	/** The dao. */
	//@Autowired
	private IAuthorDAO authorDAO;

	private static Logger logger = Logger.getLogger(AuthorService.class
			.getName());

	public void add(Author author) throws ServiceException {
		ServiceUtils.assertNotNullEntity(author, "author");
		try {
			authorDAO.add(author);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (add author failed): " + e);
			throw new ServiceException(e);
		}
	}	

	public void expired(long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "author");
		try {
			authorDAO.expired(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException(remove author fail): " + e);
			throw new ServiceException(e);
		}
	}

	public Author get(Long id) throws ServiceException {
		ServiceUtils.assertIsExistingId(id, "author");
		try {
			return authorDAO.get(id);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get author failed): " + e);
			throw new ServiceException(e);
		}
	}

	public void update(Author author) throws ServiceException {
		ServiceUtils.assertNotNullEntity(author, "author");
		try {
			authorDAO.update(author);
		} catch (DAOException e) {
			logger.error("TechnicalDAOException(update author failed): " + e);
			throw new ServiceException(e);
		}
	}

	
	public List<Author> getAll() throws ServiceException {
		try {
			List<Author> authors = authorDAO.getAll();
			authors.sort(new Comparator<Author>() {
				public int compare(Author comment1, Author comment2) {
					return comment1.getName().compareTo(comment2.getName());
				}
			});
			return authors;
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get all author failed): " + e);
			throw new ServiceException(e);
		}
	}

	public List<Author> getAllCurrentAuthor()throws ServiceException{
		try {
			List<Author> authors = authorDAO.getAllCurrentAuthor();
			authors.sort(new Comparator<Author>() {
				public int compare(Author comment1, Author comment2) {
					return comment1.getName().compareTo(comment2.getName());
				}
			});
			return authors;
		} catch (DAOException e) {
			logger.error("TechnicalDAOException (get all author failed): " + e);
			throw new ServiceException(e);
		}
	}

	public IAuthorDAO getAuthorDAO() {
		return authorDAO;
	}

	public void setAuthorDAO(IAuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}

}
