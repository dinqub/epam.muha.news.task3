package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.List;

/**
 * The Class Page.
 */
public class Page implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 728657788205822317L;

	/** The news. */
	public List<News> news;
	
	/** how much news in one page */

	private Long size;
	
	/** number of current page. */
	public long page;
	
	/** how much news  */
	public long totalSize;


	/**
	 * Instantiates a new page.
	 */
	public Page() {
	}

	/**
	 * Instantiates a new page.
	 *
	 * @param news - list of newsVO
	 * @param size - count of news in one page
	 * @param page - current page
	 * @param totalSize - total size of news
	 * @param index -  index of current news
	 */
	public Page(List<News> news, long size, long page, long totalSize,
			long index) {
		this.news = news;
		//this.size = size;
		this.page = page;
		this.totalSize = totalSize;
	}

//	public Page(Long countOfPage) {
		//this.size = countOfPage;
//	}

	/**
	 * Gets the news.
	 *
	 * @return the news
	 */
	public List<News> getNews() {
		return news;
	}

	/**
	 * Sets the news.
	 *
	 * @param news the new news
	 */
	public void setNews(List<News> news) {
		this.news = news;
	}

	/**
	 * Gets the size.
	 *
	 * @return the size
	 */
	public long getSize() {
		return size;
	}

	/**
	 * Sets the size.
	 *
	 * @param size the new size
	 */
	public void setSize(Long size) {
		this.size = size;
	}

	/**
	 * Gets the page.
	 *
	 * @return the page
	 */
	public long getPage() {
		return page;
	}

	/**
	 * Sets the page.
	 *
	 * @param page the new page
	 */
	public void setPage(long page) {
		this.page = page;
	}

	/**
	 * Gets the total size.
	 *
	 * @return the total size
	 */
	public long getTotalSize() {
		return totalSize;
	}

	/**
	 * Sets the total size.
	 *
	 * @param totalSize the new total size
	 */
	public void setTotalSize(long totalSize) {
		this.totalSize = totalSize;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result + (int) (page ^ (page >>> 32));
		result = prime * result + (int) (size ^ (size >>> 32));
		result = prime * result + (int) (totalSize ^ (totalSize >>> 32));
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		Page page = (Page) obj;
		if (this == null || obj == null)
			return false;
		else if (this.getNews() == null || page.getNews() == null)
			return false;
		else
		if (this.page == page.page && page.totalSize == this.totalSize)
			// if(this.getNews().equals(page) &&)
			return true;
		else
			return false;

	}

	@Override
	public String toString() {
		return "Page [news=" + news + ", size=" + size + ", page=" + page
				+ ", totalSize=" + totalSize +"]";
	}

}
