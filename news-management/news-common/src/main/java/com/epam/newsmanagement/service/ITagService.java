package com.epam.newsmanagement.service;

import java.util.List;
import java.util.Set;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * The Interface ITagLogic.
 */
public interface ITagService {
	
	
	/**
	 * Adds tag
	 *
	 * @param tag - tag
	 * @throws ServiceException the technical logic exception
	 */
	public void add(Tag tag) throws  ServiceException;

	/**
	 * Removes tag
	 *
	 * @param id - id of removing tag
	 * @throws ServiceException the technical logic exception
	 */
	public void remove(long id) throws ServiceException;
	
	/**
	 * Gets tag by id
	 *
	 * @param id - id of tag
	 * @return tag by id
	 * @throws ServiceException the technical logic exception
	 */
	public Tag get(long id) throws ServiceException ;
	

	
	/**
	 * Gets the all tag
	 *
	 * @return list of all tag
	 * @throws ServiceException the technical logic exception
	 */
	public List<Tag> getAll() throws ServiceException ;
	
	/**
	 * Update tag by id of tag
	 *
	 * @param tag - news tag
	 * @throws ServiceException the technical logic exception
	 */
	public void update(Tag tag) throws ServiceException;
	
	
    /**
     * Sets the dao.
     *
     * @param TagDAO the new dao
     */
    public void setTagDAO(ITagDAO tagDAO);

	public Set<Tag> getTagsById(List<Long> tagIdList)throws ServiceException;
}
