package com.epam.newsmanagement.dao.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;




import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.entity.User;


@Transactional("transactionManager")
public class UserDAO implements IUserDAO{
	
	private EntityManager entityManager;
 
	@SuppressWarnings("unchecked")
	public User findByUserName(String username) {
 
		List<User> users = new ArrayList<User>();
 
		users = entityManager.createQuery("select a from User a where a.username=:User")
			.setParameter("User", username)
			.getResultList();
 
		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
 
	}
	   @PersistenceContext
	    public void setEntityManager(EntityManager entityManager) {
	        this.entityManager = entityManager;
	    }
	    public EntityManager getEntityManager() {
	        return entityManager;
	    }
}
