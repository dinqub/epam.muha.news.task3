package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The Class Author.
 */
@Entity(name="Author")
@Table(name="Author")
public class Author implements Serializable {

	private static final long serialVersionUID = 4245262789860863982L;

	/** authors id. */
	@Id
	@Column(name="author_id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="AUTHOR_SEQ") 
    @SequenceGenerator(name="AUTHOR_SEQ",sequenceName="AUTHOR_SEQ", allocationSize=1) 
	private long id= 0;

	/** name of Author. */
	@Column(name="author_name")
	private String name= "";

	/** null if author work or time when he was fired. */
	@Column(name="expired")
	@Temporal(value=TemporalType.TIMESTAMP)
	private Date expired = null;

	/**
	 * Instantiates a new author.
	 */
	public Author() {
		this.id = 0;
		this.name = "";
		this.expired = null;
	//	this.news=new HashSet<News>();
	}

	/**
	 * Instantiates a new author.
	 *
	 * @param name
	 *            of Author
	 */
	public Author(String name) {
		this.id = 0;
		this.name = name;
//		this.news=new HashSet<News>();
	}

	/**
	 * Instantiates a new author.
	 *
	 * @param l
	 *            of Author
	 * @param name
	 *            of Author
	 */
	public Author(long l, String name) {
		this.id = l;
		this.name = name;
	}

	public Author(Long authorId) {
		this.id = authorId;
	}

	/**
	 * Gets the id of Author
	 *
	 * @return the id of Author
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id of Author
	 *
	 * @param id
	 *            of Author
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the name of Author
	 *
	 * @return the name of Author
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of Author
	 *
	 * @param name
	 *            of Author
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the expired of Author
	 *
	 * @return the expired of Author
	 */
	public Date getExpired() {
		return expired;
	}

	/**
	 * Sets the expired of Author
	 *
	 * @param expired
	 *            of Author
	 */
	public void setExpired(Date expired) {
		this.expired = expired;
	}

	@Override
	public String toString() {	
		return "Author [id=" + id + ", name=" + name + ", expired=" + expired+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
