package com.epam.newsmanagement.entity.vo;

import java.util.Date;

import com.epam.newsmanagement.entity.Comment;

public class CommentVO {
	
	private long id;

	private String commentText;
	
	private Date creationDate;

	private NewsVO news;
	
	public CommentVO() {
		super();
		this.id = new Long(0);
		this.commentText = "";
		this.creationDate = new Date();
		this.news = new NewsVO();
	}
	
	public CommentVO(long id, String commentText, Date creationDate, NewsVO news) {
		super();
		this.id = id;
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.news = news;
	}
	public CommentVO(Comment comment) {
		super();
		this.id = comment.getId();
		this.commentText = comment.getCommentText();
		this.creationDate = comment.getCreationDate();
		NewsVO newsVO=new NewsVO(comment.getNews());
		this.news=newsVO;
		
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((commentText == null) ? 0 : commentText.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommentVO other = (CommentVO) obj;
		if (commentText == null) {
			if (other.commentText != null)
				return false;
		} else if (!commentText.equals(other.commentText))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (id != other.id)
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		return true;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String commentText) {
		this.commentText = commentText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public NewsVO getNews() {
		return news;
	}

	public void setNews(NewsVO news) {
		this.news = news;
	}
}
