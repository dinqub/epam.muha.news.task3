package com.epam.newsmanagement.dao;

import java.util.List;
import java.util.Set;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;


public interface ITagDAO {


	/**
	 * Adds tag in database.
	 *
	 * @param tag - tag
	 * @throws DAOException the technical dao exception
	 */
	public void add(Tag tag)  throws DAOException ;

	/**
	 * Removes tag from database by tag id.
	 *
	 * @param id -id of tag
	 * @throws DAOException the technical dao exception
	 */
	public void remove(long id) throws DAOException;
	
	/**
	 * Gets tag from database by id of tag.
	 *
	 * @param id - id of tag
	 * @return tag by id
	 * @throws DAOException the technical dao exception
	 */
	public Tag get(long id) throws DAOException;
	
	/**
	 * Gets list of tag from database by id news.
	 *
	 * @param newsId - id of news
	 * @return list of tag by id of news
	 * @throws DAOException the technical dao exception
	 */

	/**
	 * Update tag in database by id of tag.
	 *
	 * @param tag - updating tag
	 * @throws DAOException the technical dao exception
	 */
	public void update(Tag tag) throws DAOException;

	/**
	 * Gets the all tag from database.
	 *
	 * @return list of all tag from database
	 * @throws DAOException the technical dao exception
	 */
	public List<Tag> getAll() throws DAOException;
		
	public Set<Tag> getTagsById(List<Long> tagIdList) throws DAOException;

}
