package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Formula;

/**
 * The Class News.
 */
@Entity(name = "News")
@Table(name = "News")
@Cacheable(false)
public class News implements Serializable {

	private static final long serialVersionUID = -4323006207752938575L;

	/** The id of News */
	@Id
	@Column(name = "news_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NEWS_SEQ")
	@SequenceGenerator(name = "NEWS_SEQ", sequenceName = "NEWS_SEQ", allocationSize = 1)
	private Long id = new Long(0);
	
    @Version
    @Column(name="version_id")
	private Long versionId=new Long(0);	

	/** The short text of News */

	@Column(name = "short_text")
	private String shortText= "";

	/** The full text of News */
	@Column(name = "full_text")
	private String fullText= "";

	/** The title of News */
	@Column(name = "title")
	private String title= "";

	/** The creation date of News */
	@Temporal(value = TemporalType.TIMESTAMP)
	@Column(name = "creation_date",updatable = false)
	private Date creationDate= null;

	/** The modification date of News */
	@Temporal(value = TemporalType.DATE)	
	@Column(name = "modification_date")
	private Date modificationDate = null;

	@BatchSize(size = 100)
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "news_tag", joinColumns = @JoinColumn(name = "news_id"),
	inverseJoinColumns = @JoinColumn(name = "tag_id"))
	private Set<Tag> tags = new HashSet<Tag>();

	@BatchSize(size = 100)
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "news",cascade = {CascadeType.ALL})
	@OrderBy("creationDate ASC")
	private Set<Comment> comments= new HashSet<Comment>();

	@BatchSize(size = 100)
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name="news_author",joinColumns = @JoinColumn(name="news_id"),
	   inverseJoinColumns = @JoinColumn(name="author_id"))
	private Set<Author> author = new HashSet<Author>();
	

	//@Column(name="(select count(*) from Comments  where comments.news_id=news.news_id)",insertable = false, updatable = false)
	@Formula("(select count(*) from Comments  where comments.news_id=news_id)")
	public int countComment=0;
	

	public int getCountComment() {
		return  countComment;
	}	
	/*@PostLoad
	public int getCountComment() {
		int size=comments.size();
		return  size;
		
	}*/
	
	public void setCountComment(int countComment) {
		this.countComment = countComment;
	}

	/**
	 * Instantiates a new news.
	 */
	public News() {	}

	public News(Set<Tag> tags, Set<Author> authors) { // TODO: remove from here to VO or DTO
		this.id = null;
		this.shortText = null;
		this.fullText = null;
		this.title = null;
		this.creationDate = null;
		this.modificationDate = null;
		this.author = authors;
		this.tags = tags;
		this.comments = null;
	}

	/**
	 * Instantiates a new news.
	 *
	 * @param name
	 *            of news
	 */
	public News(String news) {
		this.shortText = news;
		this.fullText = news;
		this.title = news;
		this.creationDate = new Date(1);
		this.modificationDate = new Date(1);
		this.author = new HashSet<Author>();
		this.tags = new HashSet<Tag>();
		this.comments = new HashSet<Comment>();
		this.versionId=new Long(0);
	}

	/**
	 * Instantiates a new news.
	 *
	 * @param id
	 *            of News
	 * @param shortText
	 *            of News
	 * @param fullText
	 *            of News
	 * @param title
	 *            of News
	 * @param creationDate
	 *            of News
	 * @param modificationDate
	 *            of News
	 */
	public News(long id, String shortText, String fullText, String title,
			Date creationDate, Date modificationDate) {
		super();
		this.id = id;
		this.shortText = shortText;
		this.fullText = fullText;
		this.title = title;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
		this.comments = new HashSet<Comment>();
	}

	public News(String shortText, String fullText, String title,
			Date creationDate, Date modificationDate) {
		this.shortText = shortText;
		this.fullText = fullText;
		this.title = title;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public News(Long newsId) {
		super();
		this.id = newsId;
		this.shortText = "";
		this.fullText = "";
		this.title = "";
		this.creationDate = null;
		this.modificationDate = null;
		this.author = new HashSet<Author>();
		this.tags = new HashSet<Tag>();
		this.comments = new HashSet<Comment>();
	}


	/**
	 * Gets the short text of News
	 *
	 * @return the short text of News
	 */
	public String getShortText() {
		return shortText;
	}

	/**
	 * Sets the short text of News
	 *
	 * @param shortText
	 *            of News
	 */
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	/**
	 * Gets the full text of News
	 *
	 * @return the full text of News
	 */
	public String getFullText() {
		return fullText;
	}

	/**
	 * Sets the full text of News
	 *
	 * @param fullText
	 *            of News
	 */
	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	/**
	 * Gets the title of News
	 *
	 * @return the title of News
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title of News
	 *
	 * @param title
	 *            of News
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the id of News
	 *
	 * @return the id of News
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id of News
	 *
	 * @param id
	 *            of News
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the creation date of News
	 *
	 * @return the creation date of News
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creation date of News
	 *
	 * @param creationDate
	 *            of News
	 */
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the modification date of News
	 *
	 * @return the modification date of News
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * Sets the modification date of News
	 *
	 * @param modificationDate
	 *            of News
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result
				+ ((versionId == null) ? 0 : versionId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (versionId == null) {
			if (other.versionId != null)
				return false;
		} else if (!versionId.equals(other.versionId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", versionId=" + versionId + ", shortText="
				+ shortText + ", fullText=" + fullText + ", title=" + title
				+ ", creationDate=" + creationDate + ", modificationDate="
				+ modificationDate + ", tags=" + tags + ", comments="
				+ comments + ", author=" + author + ", countComment="
				+ countComment + "]";
	}

	public Set<Tag> getTags() {
		return tags;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(HashSet<Comment> comments) {
		this.comments = comments;
	}

	public void addTag(Tag tag) {
		tags.add(tag);
	}

	public void removeTag(Tag tag) {
		this.tags.remove(tag);
	}

	public void addTags(Set<Tag> tags) {
		this.tags.clear();
		for (Tag tag : tags) {
			this.tags.add(tag);
		}
	}


	public void addAuthor(Author author) {		
		if (!this.author.isEmpty()) {			
			this.author.clear();
			this.author.add(author);
		}
		else{
			this.author.add(author);
		}
	}

	public void removeAuthor(Author author) {
		this.author.remove(author);
	}

	public void addComment(Comment comment) {
		comments.add(comment);
	}

	public Set<Author> getAuthor() {
		return author;
	}

	public void setAuthor(Set<Author> author) {
		this.author = author;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public void setInformation(News news) {
		this.shortText = news.getShortText();
		this.fullText = news.getFullText();
		this.title = news.getTitle();
		this.modificationDate = news.getModificationDate();
		this.versionId=news.getVersionId();
	}

	public Long getVersionId() {
		return versionId;
	}

	public void setVersionId(Long versionId) {
		this.versionId = versionId;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}



}
