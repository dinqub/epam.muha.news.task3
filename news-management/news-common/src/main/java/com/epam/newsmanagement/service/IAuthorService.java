package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;


/**
 * The Interface IAuthorLogic.
 */
public interface IAuthorService {

	/**
	 * Add author
	 *
	 * @param author - author
	 * @throws ServiceException the technical logic exception
	 */
	public void add(Author author) throws ServiceException;
	
	/**
	 * Removes author
	 *
	 * @param id - id of author
	 * @throws ServiceException the technical logic exception
	 */
	public void expired(long id) throws ServiceException;
	

	/**
	 * Gets author by id
	 *
	 * @param id - id of author
	 * @return author by id
	 * @throws ServiceException the technical logic exception
	 */
	public Author get(Long id) throws ServiceException;
	
	/**
	 * Gets author by news id.
	 *
	 * @param newsId - id of news
	 * @return author by news id
	 * @throws ServiceException the technical logic exception
	 */	
	/**
	 * Update author
	 *
	 * @param author - new author
	 * @throws ServiceException the technical logic exception
	 */
	public void update(Author author) throws ServiceException;
	
   	/**
	    * Gets the all author
	    *
	    * @return list of all authors
	    * @throws ServiceException the technical logic exception
	    */
	   public List<Author> getAll() throws ServiceException;
	   

	/**
	 * Sets the dao.
	 *
	 * @param IAuthorDAO the new dao
	 */
	public void setAuthorDAO(IAuthorDAO IAuthorDAO);


	public List<Author> getAllCurrentAuthor() throws ServiceException;


	

}
