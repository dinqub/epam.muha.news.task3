<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<link href="<s:url value="/resources/css/table.css"/>" rel="stylesheet" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script type="text/javascript" src="http://scriptjava.net/source/scriptjava/scriptjava.js"></script>

<title><tiles:getAsString name="title" /></title>
</head>
<body style="margin: 0; padding: 0;height: 93%;" id="bodyId">

	<div class="header">
		<tiles:insertAttribute name="header" />
	</div>
	<div class="main">
			<div class="menu" style="background-color: darkgray;width: 18%;border: 2px solid white;">
				<tiles:insertAttribute name="menu" />
			</div>
		<div class="content">
			<tiles:insertAttribute name="content" />
		</div>
	</div>
	<div class="footer">
		<tiles:insertAttribute name="footer" />
	</div>
</body>
</html>