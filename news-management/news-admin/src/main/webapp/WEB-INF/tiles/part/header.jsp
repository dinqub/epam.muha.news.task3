<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="local"%>


<c:url value="/logout" var="logoutUrl" />

<div >

	
	<div align="right" style="  float:right;  margin-right: 10;">
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
		<c:if test="${pageContext.request.userPrincipal.name != null}">
			
				<a> <local:message code="label.Hello" /> 
					${pageContext.request.userPrincipal.name}
				</a> <input type="submit" name="submit" 
				style=" padding-left: 25; padding-right: 25; padding-bottom: 5; padding-top: 5;"
					value="<local:message code="label.Logout" />" />
		
		</c:if>
	</form>
		</div>
			
		<h1 style="color: blue; margin-left: 21px;">
			<local:message code="label.NewsPortal" />
		</h1>
</div>
<div align="right">
   
    <a href="?lang=en&<%=request.getQueryString() %>"><local:message code="label.en" /></a>
     <a href="?lang=ru&<%=request.getQueryString() %>"><local:message code="label.ru" /></a>

</div>
