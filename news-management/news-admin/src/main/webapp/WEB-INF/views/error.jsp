<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Error page</title>
<link rel="stylesheet" href="<s:url value="/resources/css/error.css" />"/>
</head>
<body>
	<div class="error">
		<h1>Error page:</h1>		
	<c:if test="${not empty message}">
		<h1>${message}</h1>
	</c:if>
	
	<c:if test="${empty message}">
		<h1>Page not found</h1>
	</c:if>

	</div>
</body>
</html>
