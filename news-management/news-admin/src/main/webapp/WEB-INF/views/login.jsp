<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="local"%>

<fmt:requestEncoding value="utf-8" />
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="resource.pagecontent" />
<html>
<head>
<title>News portal</title>
<link href="<s:url value="/resources/css/login.css"/>" rel="stylesheet" />
</head>
<body onload='document.loginForm.j_username.focus();'>

	<table  border="3" bordercolor="grey" rules="cols" width="100%" height="100%">
		<tr height="29%">
			<td></td>
		</tr>
		<tr>
			<td><spring:url var="authUrl"
					value="/static/j_spring_security_check" />
				<form name='loginForm' action="${authUrl}" method='POST'>

					<div align="center">
						<c:if test="${not empty error}">
							<div class="error">${error}</div>
						</c:if>
						<c:if test="${not empty msg}">
							<div class="msg">${msg}</div>
						</c:if>
						<div style="display: inline-flex;">
							<div style="width: 100;">
								<a style="float: left;"><local:message code="label.Login" /></a>
							</div>
							<div>
								<input name="j_username" type="text" class="inputs" />
							</div>
						</div>
					</div>

					<div align="center">
						<div style="display: inline-flex;">
							<div style="width: 100;"><a style="float: left;"><local:message code="label.Password" /></a></div>
							 <div><input	name="j_password" type="password" class="input" /></div>
						</div>
					</div>
					<div align="center">
						<input type="submit" name="submit"
							value="<local:message code="label.Login" />" class="but" /> <input
							type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
					</div>
				</form></td>
		</tr>
		<tr align="center">
			<td></td>
		</tr>
	</table>

</body>
</html>