
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="local"%>

<link href="<s:url value="/resources/css/tags.css" />" rel="stylesheet" />

<script src="<s:url value="/resources/js/tags.js" /> "type="text/javascript"></script>


<div class="mainClass" >

 <c:forEach var="tag" items="${currenttag}">		
	<div class="row">
		<form action="updatetag" method="post" name="updateForm${tag.id}" id="Form${tag.id}" >
		<input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" />
   		 <label><local:message code="label.TAG" /></label>
		
		 <input type="hidden" name="tagId" value="${tag.id}" id="id${tag.id}" form="Form${tag.id}"/>
		 
		 <input	type="text" value="${tag.name}" id="${tag.id}" disabled name="tagName" form="Form${tag.id}"></input>
		
		 <a	href="#" id="edit${tag.id}"	onclick="changeAccess(${tag.id})" class="display"><local:message code="label.edit" /></a>
	 	
	 	 <a href="javascript: updatesubmitform(${tag.id},'<local:message code="label.updateTagConfirm" />')" id="update${tag.id}" class="displaynone"><local:message code="label.update" /></a> 
	 	 
		 <a href="deletetag?tagId=${tag.id}" id="delete${tag.id}" class="displaynone" onClick="return window.confirm('<local:message code="label.deleteTagConfirm" />')"><local:message	code="label.delete" /></a> 
		
		<a href="#" onclick="changeAccess(${tag.id})" id="cancel${tag.id}" class="displaynone"><local:message code="label.cancel"/></a>
		
		</form>					
	</div>
		
	</c:forEach>

	<div class="row">
		<form action="addtag" method="post" name="myform">
		<input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" />
			<label><local:message code="label.addTag" /></label> <input
				type="text" name="tagName" id="saveboxId" required="required"></input> <a
				href="javascript: submitform('<local:message	code="label.emptyField" />')"><local:message code="label.save" /></a>
		</form>
	</div>
</div>


   
   
   
   