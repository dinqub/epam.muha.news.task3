
function changeAccess(param) {
	var toggle = function(el) {
		  el.className = (el.className == 'displaynone') ? 'display' : 'displaynone'
	};

	var currentState = document.getElementById(param).disabled;
	document.getElementById(param).disabled = !currentState;

	toggle(document.getElementById("edit" + param));
	toggle(document.getElementById("update" + param));
	toggle(document.getElementById("delete" + param));
	toggle(document.getElementById("cancel" + param));
}

function updatesubmitform(param, string) {
	x = confirm(string);
	if (x == true) {
		document.getElementById("Form" + param).submit();
	}
}

function submitform(string) {
	var el=document.getElementById("saveboxId");
	if(el.value!="")
	{document.myform.submit();}
	else{
		alert(string);
	}
}