package com.epam.newsmanagement.utils;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * This class need to configured my application from properties file.
 * @author Dzmitry_Mukha1
 *
 */

public class ConfigurationManager {
	private static Locale currentEN = new Locale("en", "EN");
	private static Locale currentRU = new Locale("ru", "RU");
	
	private final static ResourceBundle resourceBundleEN = ResourceBundle.getBundle("messages", currentEN);
	
	private final static ResourceBundle resourceBundleRU = ResourceBundle.getBundle("messages", currentRU);
	
	private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("prop");

	public ConfigurationManager() {
	}

	public static String getProperty(String key, String lang) {
		switch (lang) {
		case "ru":
			return resourceBundleRU.getString(key);
		case "en":
			return resourceBundleEN.getString(key);
		default:
			return resourceBundleEN.getString(key);
		}
	}
	public static String getProperty(String key) {
		
		return resourceBundle.getString(key);
	
	}
}