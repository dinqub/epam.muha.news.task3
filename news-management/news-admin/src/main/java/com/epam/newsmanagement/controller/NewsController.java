package com.epam.newsmanagement.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Filter;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Page;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.INewsManagerService;
import com.epam.newsmanagement.utils.ConfigurationManager;

/**
 * This controller need to view list of news or one of news.
 * 
 * @author Dzmitry_Mukha1
 *
 */
@Controller
@RequestMapping("/admin")
public class NewsController extends ErrorController {

	@Autowired
	//@Qualifier("newsManagerService")
	public INewsManagerService newsManagerService;

	final static String NEXT = "next";
	final static String PREV = "prev";

	@RequestMapping(value = "/navigationnews", method = RequestMethod.GET)
	public String navigationNews(
			RedirectAttributes redirectAttributes,
			@RequestParam(value = "navigationType", required = false) String navigationType,
			@RequestParam(value = "newsId", required = false) long newsId,
			@RequestParam(value = "newsIdInFilter", required = false) long newsIdInFilter,
			Model model, HttpServletRequest request) throws ServiceException {

		Filter filter = (Filter) request.getSession().getAttribute("filter");

		long valueOfNavigationType = 0;
		if (navigationType.equals(NEXT)) {
			valueOfNavigationType = 1;
		}
		if (navigationType.equals(PREV)) {
			valueOfNavigationType = -1;
		}
		News news = newsManagerService.getFilterNews(filter, newsIdInFilter
				+ valueOfNavigationType);
		redirectAttributes.addAttribute("newsIdInFilter", newsIdInFilter
				+ valueOfNavigationType);
		redirectAttributes.addAttribute("newsId", news.getId());

		return "redirect:/admin/newsview";
	}

	@RequestMapping(value = "/newsview", method = RequestMethod.GET)
	public String newsView(Model model, HttpServletRequest request,
			@RequestParam(value = "newsId", required = false) long newsId,
			@RequestParam(value = "newsIdInFilter", required = false) long newsIdInFilter)
			throws ServiceException {

		Filter filter = (Filter) request.getSession().getAttribute("filter");

		Long contOfNews = newsManagerService.getCountOfFilterPage(filter);
		model.addAttribute("totalNews", contOfNews);
		model.addAttribute("newId", newsId);
		model.addAttribute("newsIdInFilter", newsIdInFilter);

		News news = newsManagerService.getNews(newsId);
		model.addAttribute("newsVO", news);

		return "newsView";
	}

	@RequestMapping(value = "/news", method = RequestMethod.GET)
	public String news(
			RedirectAttributes redirectAttributes,
			@RequestParam(value = "newsId", required = false) Long newsId,
			@RequestParam(value = "pageId", required = false) Long pageId,
			@RequestParam(value = "newsOnPage", required = false) Long newsOnPage,
			@RequestParam(value = "rowOnPageId", required = false) int rowOnPageId,
			Model model) throws ServiceException {

		long newsIdInFilter = (pageId - 1) * newsOnPage + rowOnPageId;

		redirectAttributes.addAttribute("newsIdInFilter", newsIdInFilter);
		redirectAttributes.addAttribute("newsId", newsId);

		return "redirect:/admin/newsview";
	}

	@RequestMapping(value = "/listnews", method = RequestMethod.GET)
	public String listNews(Model model,
			@RequestParam(value = "pageId", required = false) Long pageId,
			HttpServletRequest request) throws ServiceException {

		Filter filter = (Filter) request.getSession().getAttribute("filter");

		if (filter == null) {
			filter = new Filter();
			request.getSession().setAttribute("filter", filter);
		}
		if (pageId == null) {
			filter = new Filter();
			request.getSession().setAttribute("filter", filter);
			pageId = 1L;
		}
		

		Page page = new Page();
		page.setSize(Long.parseLong(ConfigurationManager
				.getProperty("pageCount")));
		List<News> news = newsManagerService.getFilterNewsList(filter,
				pageId, page.getSize());

		page.setNews(news);
		page.setPage(pageId);
		page.setTotalSize(newsManagerService.getCountOfFilterPage(filter));

		model.addAttribute("page", page);

		List<Author> authors = newsManagerService.getAllAuthors();
		List<Tag> tags = newsManagerService.getAllTags();

		model.addAttribute("authors", authors);
		model.addAttribute("tags", tags);

		model.addAttribute("filter", filter);

		return "listNews";
	}

	@RequestMapping(value = "/reset", method = RequestMethod.GET)
	public String reset(Model model, RedirectAttributes redirectAttributes,
			@RequestParam(value = "pageId", required = false) long pageId,
			HttpServletRequest request) throws ServiceException {

		Filter filter = new Filter();

		request.getSession().setAttribute("filter", filter);
		redirectAttributes.addAttribute("pageId", 1L);

		return "redirect:/admin/listnews";
	}

	@RequestMapping(value = "/filter", method = RequestMethod.POST)
	public String filter(
			Model model,
			RedirectAttributes redirectAttributes,
			@RequestParam(value = "authorId", required = false) long authorId,
			@RequestParam(value = "tagIdList", required = false) List<Long> tagIdList,
			HttpServletRequest request) throws ServiceException {

		Filter filter = new Filter(authorId, tagIdList);

		redirectAttributes.addAttribute("pageId", 1L);

		request.getSession().setAttribute("filter", filter);

		return "redirect:/admin/listnews";
	}

	@RequestMapping(value = "/deletenews", method = RequestMethod.GET)
	public String deleteNews(
			Model model,
			RedirectAttributes redirectAttributes,
			@RequestParam(value = "pageId", required = false) long pageId,
			@RequestParam(value = "deleteIdList", required = false) List<Long> deleteIdList)
			throws ServiceException {

		if (deleteIdList != null) {
			newsManagerService.removeNews(deleteIdList);
		}
		redirectAttributes.addAttribute("pageId", pageId);

		return "redirect:/admin/listnews";
	}

}
