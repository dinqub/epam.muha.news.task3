package com.epam.newsmanagement .controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ITagService;

/**
 * This controller need to view list of tag and delete,update and add tags.
 * @author Dzmitry_Mukha1
 *
 */
@Controller
@RequestMapping("/admin")
public class TagController extends ErrorController {

	@Autowired
	//@Qualifier("tagService")
	public ITagService tagService;

	@RequestMapping(value = "/tagview", method = RequestMethod.GET)
	public String tagView(Model model) throws ServiceException {

		List<Tag> tags=tagService.getAll();
		
		model.addAttribute("currenttag", tags);

		return "tags";
	}

	@RequestMapping(value = "/deletetag", method = RequestMethod.GET)
	public String deleteTags(Model model,
			@RequestParam(value = "tagId", required = false) Long tagId)
			throws ServiceException {

		if (tagId != null) {
			tagService.remove(tagId);
		} 

		return "redirect:/admin/tagview";
	}

	@RequestMapping(value = "/addtag", method = RequestMethod.POST)
	public String addTags(Model model,
			@RequestParam(value = "tagName", required = false) String tagName)
			throws ServiceException {

		if (StringUtils.isBlank(tagName)) {
			throw new ServiceException("Tag name not correct. Tag name null or empty");
		}
		Tag tag = new Tag(tagName);
		tagService.add(tag);
		return "redirect:/admin/tagview";
	}

	@RequestMapping(value = "/updatetag", method = RequestMethod.POST)
	public String updateTags(Model model,
			@RequestParam(value = "tagName", required = false) String tagName,
			@RequestParam(value = "tagId", required = false) Long tagid)
			throws ServiceException {

		if (StringUtils.isBlank(tagName)) {
			throw new ServiceException("Tag name not correct. Tag name null or empty");
		}
		Tag tag = new Tag(tagid, tagName);
		tagService.update(tag);
		

		return "redirect:/admin/tagview";
	}

}
