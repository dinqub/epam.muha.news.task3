package com.epam.newsmanagement.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.INewsManagerService;
import com.epam.newsmanagement.utils.LocaleDateEnum;

/**
 * This controller need delete,update and add news.
 * 
 * @author Dzmitry_Mukha1
 *
 */
@Controller
@RequestMapping("/admin")
public class NewsUpdateController extends ErrorController {

	@Autowired
	// @Qualifier("newsManagerService")
	private INewsManagerService newsManagerService;

	@RequestMapping(value = "/editnews", method = RequestMethod.GET)
	public String editNews(
			@RequestParam(value = "newsId", required = false) long newsId,
			@RequestParam(value = "error", required = false) Boolean error,
			Model model) throws ServiceException {

		News newsVO = new News();

		newsVO = newsManagerService.getNews(newsId);
		newsVO.setCreationDate(newsVO.getModificationDate());
		newsVO.setModificationDate(new Date());

		List<Author> authors = newsManagerService.getAllAuthors();
		List<Tag> tags = newsManagerService.getAllTags();

		String err;
		if (error != null) {
			err = new String("some");
			model.addAttribute("error", err);
		} else {
			err = null;
			model.addAttribute("error", err);
		}
		
		model.addAttribute("editedNews", newsVO);
		model.addAttribute("authors", authors);
		model.addAttribute("tags", tags);

		return "updatenews";
	}

	@RequestMapping(value = "/updatenews", method = RequestMethod.POST)
	public String updateNews(
			Model model,
			HttpServletRequest request,
			@ModelAttribute("editedNews") News news,
			@RequestParam(value = "language", required = false) String language,
			@RequestParam(value = "date", required = false) String dates,
			@RequestParam(value = "versionId", required = false) long versionId,
			@RequestParam(value = "authorId", required = false) long authorId,
			@RequestParam(value = "tagIdList", required = false) List<Long> tagIdList,
			@RequestParam(value = "newsId", required = false) long newsId)
			throws ParseException, ServiceException {

		Date date = LocaleDateEnum.byLocaleName(language).parseDate(dates);
		// Date dateWhenEnters =
		// LocaleDateEnum.byLocaleName(language).parseDate(dateWhenEnter);
		news.setModificationDate(date);
		// news.setCreationDate(dateWhenEnters);
		news.setVersionId(versionId);

		// TODO
		try {
			newsManagerService.updateNews(news, tagIdList, authorId);
		} catch (ServiceException e) {
			model.addAttribute("error", true);
			model.addAttribute("newsId", newsId);
			return "redirect:/admin/editnews";
		}

		return "redirect:/admin/listnews";
	}

	@RequestMapping(value = "/addnewspage", method = RequestMethod.GET)
	public String addNewsPage(Model model) throws ServiceException {

		List<Author> authors = newsManagerService.getAllAuthors();
		List<Tag> tags = newsManagerService.getAllTags();
		Date today = new Date();
		News addedNews = new News();
		model.addAttribute("addedNews", addedNews);
		model.addAttribute("todays", today);
		model.addAttribute("authors", authors);
		model.addAttribute("tags", tags);

		return "addnews";
	}

	@RequestMapping(value = "/addnews", method = RequestMethod.POST)
	public String addNews(
			Model model,
			@ModelAttribute("addedNews") News news,
			@RequestParam(value = "language", required = false) String language,
			@RequestParam(value = "date", required = false) String dates,
			@RequestParam(value = "authorId", required = false) Long authorId,
			@RequestParam(value = "tagIdList", required = false) List<Long> tags)
			throws ParseException, ServiceException {

		Date date = LocaleDateEnum.byLocaleName(language).parseDate(dates);

		news.setModificationDate(date);
		news.setCreationDate(date);

		newsManagerService.addNews(news, tags, authorId);

		return "redirect:/admin/listnews";
	}

}
