/*
 * 
 */
package com.epam.newsmanagement.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IAuthorService;

/**
 * This controller need to view list of author and delete,update and add
 * authors.
 * 
 * @author Dzmitry_Mukha1
 *
 */

@Controller
@RequestMapping("/admin")
public class AuthorController extends ErrorController {

	@Autowired
	//@Qualifier("authorService")
	public IAuthorService authorService;

	@RequestMapping(value = "/authorview", method = RequestMethod.GET)
	public String authorView(Model model) throws ServiceException {

		List<Author> authors = authorService.getAllCurrentAuthor();
		model.addAttribute("currentauthor", authors);

		return "authors";
	}

	@RequestMapping(value = "/expireauthor", method = RequestMethod.GET)
	public String expireAuthor(Model model,
			@RequestParam(value = "authorId", required = false) Long authorId)
			throws ServiceException {

		if (authorId != null) {
			authorService.expired(authorId);
		}
		return "redirect:/admin/authorview";
	}

	@RequestMapping(value = "/addauthor", method = RequestMethod.POST)
	public String addAuthor(
			Model model,
			@RequestParam(value = "authorName", required = false) String authorName)
			throws ServiceException {

		if (StringUtils.isBlank(authorName)) {
			throw new ServiceException(
					"Author name not correct. Author name null or empty");
		}
		Author tag = new Author(authorName);
		authorService.add(tag);

		return "redirect:/admin/authorview";
	}

	@RequestMapping(value = "/updateauthor", method = RequestMethod.POST)
	public String updateAuthor(
			Model model,
			@RequestParam(value = "authorName", required = false) String authorName,
			@RequestParam(value = "authorId", required = false) long authorid)
			throws ServiceException {

		if (StringUtils.isBlank(authorName)) {
			throw new ServiceException(
					"Author name not correct. Author name null or empty");
		}
		Author author = new Author(authorid, authorName);
		authorService.update(author);

		return "redirect:/admin/authorview";
	}
}
