package com.epam.newsmanagement.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * This controller need to login.
 * @author Dzmitry_Mukha1
 *
 */
@Controller
public class LoginController extends ErrorController{

	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String defaultCommand(Model model, HttpServletRequest request) {

		Locale.setDefault(Locale.ENGLISH);

		return "redirect:/admin/listnews";
	}
	
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String adminPageCommand(Model model, HttpServletRequest request) {

		Locale.setDefault(Locale.ENGLISH);

		return "redirect:/admin/listnews";
	}


	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model,
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {

		Locale.setDefault(Locale.ENGLISH);
		if (error != null) {
			model.addAttribute("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addAttribute("msg", "You've been logged out successfully.");
		}

		return "login";
	}
}
