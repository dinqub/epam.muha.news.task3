package com.epam.newsmanagement.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ICommentService;

/**
 * This controller need to view list of comments and delete,update and add commentss.
 * @author Dzmitry_Mukha1
 *
 */

@Controller
@RequestMapping("/admin")
public class CommentController extends ErrorController {
	
	@Autowired
	public ICommentService commentService;
	
	@RequestMapping(value = "/addcomment", method = RequestMethod.POST)
	public String addCommentToNews(
			RedirectAttributes redirectAttributes,
			@RequestParam(value = "newsId", required = false) Long newsId,
			@RequestParam(value = "newsIdInFilter", required = false) Long newsIdInFilter,
			@RequestParam(value = "commentText", required = false) String commentText,
			Model model) throws ServiceException {
		
		if (!StringUtils.isBlank(commentText)) {
			Comment comment = new Comment(commentText, newsId);
			commentService.add(comment);
		}
		redirectAttributes.addAttribute("newsIdInFilter", newsIdInFilter);
		redirectAttributes.addAttribute("newsId", newsId);

		return "redirect:/admin//newsview";
	}

	@RequestMapping(value = "/deletecomment", method = RequestMethod.GET)
	public String deleteComment(
			RedirectAttributes redirectAttributes,
			@RequestParam(value = "commentId", required = false) Long commentId,
			@RequestParam(value = "newsId", required = false) Long newsId,
			@RequestParam(value = "newsIdInFilter", required = false) Long newsIdInFilter,
			Model model, HttpServletRequest request) throws ServiceException {

		redirectAttributes.addAttribute("newsIdInFilter", newsIdInFilter);
		redirectAttributes.addAttribute("newsId", newsId);
		if(commentId!=null)
		commentService.remove(commentId);

		return "redirect:/admin//newsview";
	}
}
