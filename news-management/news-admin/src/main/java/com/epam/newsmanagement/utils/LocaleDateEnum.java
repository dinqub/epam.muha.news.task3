package com.epam.newsmanagement.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * This enum need to format date .
 * @author Dzmitry_Mukha1
 *
 */
public enum LocaleDateEnum {
	
	EN("en", ConfigurationManager.getProperty("data.patern","en")), 
	RU("ru",  ConfigurationManager.getProperty("data.patern","ru"));
	
	final String localeName;
	final String format;

	private LocaleDateEnum(String localeName, String format) {
		this.localeName = localeName;
		this.format = format;
	}
	
	public String formatDate(Date date) {
		DateFormat df = new SimpleDateFormat(format,
				Locale.ENGLISH);
		return df.format(date);		
	}
	
	public Date parseDate(String date) throws ParseException {
		DateFormat df = new SimpleDateFormat(format,
				Locale.ENGLISH);
		return df.parse(date);		
	}
	
	public static LocaleDateEnum byLocaleName(String localeName) {
		for (LocaleDateEnum e: values()) {
			if (e.localeName.equals(localeName)) {
				return e;
			}
		}
		throw new IllegalArgumentException("Unknown locale: " + localeName);
	}
}
