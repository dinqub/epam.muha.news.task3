package com.epam.newsmanagement.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/**
 * This controller need to handle errors from controllers.
 * @author Dzmitry_Mukha1
 *
 */
@Controller
public class ErrorController {

	@ExceptionHandler(Exception.class)
	public ModelAndView handleIOException(Exception exception) {
		ModelAndView modelAndView = new ModelAndView("error");
		modelAndView.addObject("message", exception.getMessage());
		return modelAndView;

	}
}