<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="local"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="<s:url value="/resources/css/addnewes.css" />"
	rel="stylesheet" />
<link rel="stylesheet" type="text/css"
	href="<s:url value="/resources/css/dropdown.css" />">
	
<script src="<s:url value="/resources/js/dropdown.js" />"	type="text/javascript"></script>
<script src="<s:url value="/resources/js/calendar.js" />"	type="text/javascript"></script>

<form:form action="updatenews"  modelAttribute="editedNews" name="addForm"  method="POST">

	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />
			<local:message code="data.patern" var="h" />
		 <form:input type="hidden" name="newsId" path="id" value="${editedNews.id}" />
	<input type="hidden" name="dateWhenEnter" value="<fmt:formatDate value="${editedNews.creationDate}" 
			type="both" pattern="${h}" />" /> 
	<div class="row" style="margin-top: 5%;">
		<div class="lab">
			<label><local:message code="label.title" /></label>
		</div>
		<div class="title">
			<form:input path="title" required="required" class="titles"/>
  			<form:errors path="title" cssClass="error" />
			<%-- <input name="title" type="text" style="width: 100%;"
				value="${newsVO.news.title}"></input> --%>
		</div>
	</div>
	
	<div class="row">
		<div class="lab">
			<label><local:message code="label.date" /></label>
		</div>
		<div class="date">
			<input type="hidden" name="language" 
				value="${pageContext.response.locale}" />
			<input type="hidden" value="<local:message code="patern" />" id="paternId"/>
			<local:message code="data.patern" var="h" />
			<input type="text" id="datepicker" required="required" name="date" 
			value="<fmt:formatDate value="${editedNews.modificationDate}" 
			type="both" pattern="${h}" />">
		
		</div>
	</div>
	<div class="row">
		<div class="lab">
			<label><local:message code="label.brief" /></label>
		</div>
		<div class="brief">
		<form:textarea name="brief"  required="required" path="shortText" />
  			<form:errors path="shortText" cssClass="error" />
			<%-- <textarea name="brief">${newsVO.shortText}</textarea>	 --%>		
		</div>
	</div>
	<div class="row">
		<div class="lab">
			<label><local:message code="label.content" /></label>
		</div>
		<div class="contents">
		<form:textarea name="content"  required="required" path="fullText" />
  			<form:errors path="fullText" cssClass="error" />
			<%-- <textarea name="content">${newsVO.fullText}</textarea> --%>
		</div>

	</div>

	<div class="center">
		<div class="firstselect">
			<select name="authorId">
				<c:if test="${newsVO.author.toArray()[0].id == 0 }">selected</c:if>
				<c:forEach items="${authors}" var="author">
					<c:if
						test="${empty author.expired or author.id==newsVO.author.toArray()[0].id}">
						<option value="${author.id}"
							<c:if test="${editedNews.author.toArray()[0].id == author.id }"> 
          					 selected
        					  </c:if>>${author.name}</option>
					</c:if>
				</c:forEach>
			</select>
		</div>
		<input type="hidden" name="tagloc"
			value="<local:message code="label.Tag"/>" />
		<div class="abs">
			<div class="multiselect">
				<div class="selectBox" id="selectBox">
					<select form="filterForm">
						<option id="tag"></option>
					</select>
					<div class="overSelect"></div>
				</div>
				<div id="checkboxes" class="checkboxes">
					<c:forEach items="${tags }" var="tag">
						<label class="checkBTag" for="${tag.id}"> <input
							type="checkbox" name="tagIdList" value="${tag.id}"
							class="checkBTag" id="${tag.name}"
							<c:if test="${ not empty editedNews.tags && editedNews.tags.contains(tag) }">
        						 checked
       							 </c:if> />
							${tag.name}
						</label>
					</c:forEach>
				</div>
			</div>
		</div>


		<div class="buttonpanel">
			<input type="submit" value="<local:message code="label.save" />" />
		</div>
	</div>
</form:form>
