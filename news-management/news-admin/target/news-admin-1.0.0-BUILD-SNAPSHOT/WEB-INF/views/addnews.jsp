<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="local"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="<s:url value="/resources/css/addnewes.css" />" rel="stylesheet" />
<link href="<s:url value="/resources/css/dropdown.css" />" rel="stylesheet" />

<script src="<s:url value="/resources/js/dropdown.js" />"	type="text/javascript"></script>
<script src="<s:url value="/resources/js/calendar.js" />"	type="text/javascript"></script>


<form:form action="addnews" modelAttribute="addedNews" name="addForm"  method="POST">
   
<input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" />
	<div class="rowfirst" >
		<div class="lab">
			<label><local:message code="label.title" /></label>
		</div>
		<div class="title">
			<form:input path="title" required="required" class="titles"/>
  			<form:errors path="title" cssClass="error" />
			<!-- <input name="title" type="text" required="required" class="titles"></input> -->
		</div>

	</div>
	<div class="row">
		<div class="lab">
			<label><local:message code="label.date" /></label>
		</div>
		<div class="date">
		<input type="hidden" name="language" required="required" value="${pageContext.response.locale}"/>
		<input type="hidden" value="<local:message code="patern" />" id="paternId"/>
		<local:message code="data.patern" var="h" />
		<input type="text" id="datepicker" required="required" name="date" 
		value="<fmt:formatDate value="${todays}" 
		type="both" pattern="${h}" />"/>
	
		</div>
	</div>
	<div class="row">
		<div class="lab">
			<label><local:message code="label.brief" /></label>
		</div>
		<div class="brief">
			<form:textarea name="brief"  required="required" path="shortText" />
  			<form:errors path="shortText" cssClass="error" />
			<!-- <textarea name="brief"  required="required"></textarea> -->
		</div>

	</div>
	<div class="row">
		<div class="lab">
			<label><local:message code="label.content" /></label>
		</div>
		<div class="contents">
			<form:textarea name="content"  required="required" path="fullText" />
  			<form:errors path="fullText" cssClass="error" />
<!-- 			<textarea name="content"   required="required"></textarea>
 -->		</div>

	</div>
	
	<div>
	
	</div>
	<div class="center">
		<div class="firstselect">
			<select name="authorId" required>
			<option value="">
					<local:message code="label.Author" /></option>
				<c:forEach items="${authors}" var="author">
					<c:if test="${empty author.expired}">
						<option value="${author.id}">${author.name}</option>
					</c:if>
				</c:forEach>
			</select>
		</div>
<input type="hidden" name="tagloc"
			value="<local:message code="label.Tag"/>" />
		<div class="abs">
			<div class="multiselect">
				<div class="selectBox" id="selectBox">
					<select form="filterForm">
						<option id="tag"></option>
					</select>
					<div class="overSelect"></div>
				</div>
				<div id="checkboxes" class="checkboxes">
					<c:forEach items="${tags }" var="tag">
						<label class="checkBTag" for="${tag.id}"> <input
							type="checkbox" name="tagIdList" value="${tag.id}"
							class="checkBTag" id="${tag.name}" /> ${tag.name}
						</label>
					</c:forEach>
				</div>
			</div>
		</div>
		<div class="buttonpanel">
			<input type="submit" class="mainNews" value="<local:message code="label.save" />" />
		</div>
	</div>

</form:form>