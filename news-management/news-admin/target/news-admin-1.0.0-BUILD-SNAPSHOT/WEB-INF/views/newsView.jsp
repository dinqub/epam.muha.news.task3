<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ taglib uri="http://www.springframework.org/tags" prefix="local"%>

<link href="<s:url value="/resources/css/newsview.css"/>"
	rel="stylesheet" />

<fmt:requestEncoding value="utf-8" />
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="resource.pagecontent" />

<div class="mainNews">
	<div class="mainText">
		<div class="bottomspace">
			<div align="left">
				<a href="listnews?pageId=1"><local:message
						code="label.back" /></a>
			</div>
		</div>
		<div class="bottomspace">
			<div>
				<a class="news">${newsVO.title}</a> <a>(by
					${newsVO.author.toArray()[0].name})</a>
				<div align="right" class="right">
					<a class="date"> <local:message code="data.patern" var="h" />
						<fmt:formatDate value="${newsVO.modificationDate}" type="both"
							pattern="${h}" /></a>
				</div>
			</div>
		</div>
		<div class="shortText">
			<label>${newsVO.fullText}</label>
		</div>

		<div class="commLine">
			<div>
				<c:forEach items="${newsVO.comments}" var="comment">
					<div>
						<a class="date"> <local:message code="data.patern" var="h" />
							<fmt:formatDate value="${comment.creationDate}" type="both"
								pattern="${h}" /></a>
					</div>

					<div class="commentText">
						<div class="divRight">
							<a
								href="deletecomment?commentId=${comment.id}&newsId=${newsVO.id}&newsIdInFilter=${newsIdInFilter}"
								onClick="return window.confirm('<local:message code="label.deleteCommentConfirm"/>')">
								<button>x</button>
							</a>
						</div>
						<label>${comment.commentText}</label>
					</div>


				</c:forEach>
			</div>
			<div>
				<form action="addcomment" method="post" >
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
					<div>
						<textarea id="txtArea" rows="10" cols="70" name="commentText" required="required"
							class="commentingArea"></textarea>
					</div>
					<div class="button">
						<input type="hidden" name="newsId" value="${newsVO.id}" /> <input
							type="hidden" name="newsIdInFilter" value="${newsIdInFilter}" />

						<div class="divRight">
							<input type="submit" value="<local:message code="label.post" />"
								class="but" />
						</div>

					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="navifgationPanel">
		<c:if test="${newsIdInFilter>1 && newsIdInFilter<totalNews}">
			<div align="left">
				<a
					href="navigationnews?navigationType=prev&newsId=${newsVO.id}&newsIdInFilter=${newsIdInFilter}"><local:message
						code="label.prev" /></a>
				<div align="right" class="right">
					<a
						href="navigationnews?navigationType=next&newsId=${newsVO.id}&newsIdInFilter=${newsIdInFilter}"><local:message
							code="label.next" /></a>
				</div>
			</div>
		</c:if>
		<c:if test="${newsIdInFilter==1 && newsIdInFilter<totalNews}">
			<div align="right" class="right">
				<a
					href="navigationnews?navigationType=next&newsId=${newsVO.id}&newsIdInFilter=${newsIdInFilter}"><local:message
						code="label.next" /></a>
			</div>
		</c:if>
		<c:if test="${newsIdInFilter>1 && newsIdInFilter==totalNews}">
			<div align="left">
				<a
					href="navigationnews?navigationType=prev&newsId=${newsVO.id}&newsIdInFilter=${newsIdInFilter}"><local:message
						code="label.prev" /></a>
			</div>
		</c:if>
	</div>

</div>