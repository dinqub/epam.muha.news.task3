<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="local"%>

<link href="<s:url value="/resources/css/authors.css" />" rel="stylesheet" />

<script src="<s:url value="/resources/js/authors.js" /> "type="text/javascript"></script>

<div class="mainNews">

		<c:forEach var="author" items="${currentauthor}">
		<c:if test="${empty author.expired}">
		<div class="row">	
			
				<form action="updateauthor" method="post" name="Form${author.id}" id="Form${author.id}">
				<input type="hidden" name="${_csrf.parameterName}"	value="${_csrf.token}" />
				<label class="simpl"><local:message	code="label.author" /></label>	
					<input type="hidden" name="authorId" value="${author.id}" id="id${author.id}" form="Form${author.id}"/>
					<input type="text" class="text" name="authorName" id="${author.id}" value="${author.name}" form="Form${author.id}" disabled></input>
					 <a	href="#" id="edit${author.id}" 	onclick="changeAccess(${author.id})" class="display"><local:message code="label.edit" /></a>			
					
					 <a href="javascript: updatesubmitform(${author.id},'<local:message code="label.updateAuthorConfirm" />')" id="update${author.id}"  class="displaynone"><local:message code="label.update" /></a>
					 <a href="expireauthor?authorId=${author.id}" id="delete${author.id}" class="displaynone" onClick="return window.confirm('<local:message code="label.deleteAuthorConfirm" />')"><local:message
								code="label.expire" /></a>
					<a href="#" onclick="changeAccess(${author.id})" id="cancel${author.id}"  class="displaynone"><local:message code="label.cancel"/></a>

				</form>
		</div>
		</c:if>
		
		</c:forEach>

<div class="row">
		<form action="addauthor" method="post" name="myform">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<label class="addsimpl"><local:message
							code="label.addauthor" /></label>
				<input type="text" class="text" name="authorName" id="saveboxId" required="required"></input>
				
				<a href="javascript: submitform('<local:message	code="label.emptyField" />')"><local:message
							code="label.save" /></a>
			
		</form>
	</div>
</div>
