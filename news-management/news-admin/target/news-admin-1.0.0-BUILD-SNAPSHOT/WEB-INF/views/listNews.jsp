<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="local"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<link rel="stylesheet" type="text/css"
	href="<s:url value="/resources/css/listnews.css" />">
<link rel="stylesheet" type="text/css"
	href="<s:url value="/resources/css/dropdown.css" />">

<script src="<s:url value="/resources/js/dropdown.js" />"
	type="text/javascript"></script>
<script src="<s:url value="/resources/js/calendar.js" />"
	type="text/javascript"></script>	
<script src="<s:url value="/resources/js/listnews.js" />"	type="text/javascript"></script>


<fmt:requestEncoding value="utf-8" />


<form action="deletenews" method="get" id="deleteForm"></form>
<form action="filter" method="POST" id="filterForm"></form>
<input type="hidden" name="${_csrf.parameterName}"
	value="${_csrf.token}" form="filterForm" />
<div class="mainNews">
	<div class="center">
		<div class="firstselect">
			<select name="authorId" form="filterForm">
				<option value="0"
					<c:if test="${filter.authorId == 0 }">selected</c:if>>
					<local:message code="label.Author" /></option>
				<c:forEach items="${authors}" var="author">
					<option value="${author.id}"
						<c:if test="${filter.authorId == author.id }"> 
          					 selected
        					  </c:if>>
						${author.name}</option>
				</c:forEach>
			</select>
		</div>
		<input type="hidden" name="tagloc"
			value="<local:message code="label.Tag"/>" />
		<div class="abs">
			<div class="multiselect">
				<div class="selectBox" id="selectBox">
					<select form="filterForm">
						<option id="tag"></option>
					</select>
					<div class="overSelect"></div>
				</div>
				<div id="checkboxes" class="checkboxes">
					<c:forEach items="${tags }" var="tag">
						<label class="checkBTag" for="${tag.id}"> <input
							form="filterForm" type="checkbox" name="tagIdList"
							value="${tag.id}" class="checkBTag" id="${tag.name}"
							<c:if test="${ not empty filter.tags && filter.tags.contains(tag.id) }">
        						 checked
       							 </c:if> />
							${tag.name}
						</label>
					</c:forEach>
				</div>
			</div>
		</div>

		<div class="buttonpanel">
			<input type="submit" form="filterForm" class="filterForms"
				value="<local:message code="label.filter" />" />
		</div>
		<a href="reset?pageId=${page.page}"><button>
				<local:message code="label.reset" />
			</button></a>
	</div>


	<div class="central">
		<c:forEach items="${page.news}" var="newsVO" varStatus="theCount">

			<div class="newsLine">
				<div class="underNewsLine">
					<div>
						<a
							href="news?newsId=${newsVO.id}&rowOnPageId=${theCount.count}&pageId=${page.page}&newsOnPage=${page.size}"
							class="news">${newsVO.title}</a> <a>(by
							${newsVO.author.toArray()[0].name})</a>

						<div align="right" class="right">
							<a class="date"> <local:message code="data.patern" var="h" />
								<fmt:formatDate value="${newsVO.modificationDate}" type="both"
									pattern="${h}" /></a>
						</div>
					</div>
				</div>
				<div class="shortTextDiv">
					<a class="shortText">${newsVO.shortText}</a>
				</div>
				<div>

					<div align="right">
						<c:forEach items="${newsVO.tags}" begin="0" end="0" var="tagss">

							<a class="tagNames">${tagss.name}</a>

						</c:forEach>
						<c:forEach items="${newsVO.tags}" begin="1" var="tagss">

							<a class="tagNames">, ${tagss.name}</a>

						</c:forEach>
						<a class="comment"><local:message code="label.comment" />(${fn:length(newsVO.comments)})</a>
						<a href="editnews?newsId=${newsVO.id}"> <local:message
								code="label.edit" />
						</a> <input type="hidden" name="pageId" value="${page.page}"
							form="deleteForm" /> <input type="checkbox" name="deleteIdList"
							form="deleteForm" value="${newsVO.id}" class="checkBTag"
							id="${newsVO.id}" />
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
</div>

<div class="deleteDiv">
	<div class="deleteDivRight">
		<a
			onClick="submitDeleteform('<local:message code="label.deleteNewsConfirm" />',
									  '<local:message code="label.deleteNewsError" />')">
			<button class="deleteButton"  id="deleteButton">
				<local:message code="label.Delete" />
			</button>
		</a>
	</div>
</div>

<div align="center" class="navigation">
	<div class="Navp">
		<c:forEach begin="1" end="${(page.totalSize + page.size -1)/ page.size}" var="i">

			<c:if test="${page.page==i}">
				<a href="listnews?pageId=${i}" class="pagination">
					<button class="choseButton">${i}</button>
				</a>
			</c:if>
			<c:if test="${page.page!=i}">
				<a href="listnews?pageId=${i}" clas="pagination">
					<button>${i}</button>
				</a>
			</c:if>

		</c:forEach>
	</div>

</div>
</body>

